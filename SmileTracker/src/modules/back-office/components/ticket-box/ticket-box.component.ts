import { Component, OnInit, Input } from '@angular/core';
import { TicketsDialogComponent } from '../../dialogs/tickets-dialog/tickets-dialog.component';
import { MatDialog } from '@angular/material';
import { environment } from 'src/environments/environment';
import { PubSubService } from 'angular7-pubsub';
import { MapperService } from 'src/modules/shared/providers/mappers/mapper.service';
import { ProjectsService } from 'src/modules/shared/providers/projects/projects.service';
import { Time } from 'src/modules/shared/models/time';

@Component({
  selector: 'app-ticket-box',
  templateUrl: './ticket-box.component.html',
  styleUrls: ['./ticket-box.component.scss']
})
export class TicketBoxComponent implements OnInit {
  @Input() users: any = null;
  @Input() mode: any = null;
  @Input() project: any = null;

  // Component variable helpers
  public newAssignedTickets: any = [];
  public filterdTickets: any = [];
  public noItemFound = false;
  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C']
  };

  view: any[] = [100, 80];

  constructor (
    public dialog: MatDialog,
    public pubsub: PubSubService,
    public mapperService: MapperService,
    public projectService: ProjectsService
  ) { }

  ngOnInit () {
    this.ticketSubscribers();
    this.filterdTickets = this.project.assignedTickets;
  }

  ticketSubscribers () {

    // Map & Add new ticket to assignedTickets array
    this.pubsub.$sub('new:ticket', (data) => {
      if (data.type && data.type === 'add') {
        // Increment ID in payload before Mapping
        data.data.id = this.project.assignedTickets.length + 1;
        const ticket = this.mapperService.mapJsonToAssignedSingleTickets(data.data);
        this.project.assignedTickets.push(ticket);
      } else {
        // Edit mode
        this.newAssignedTickets = this.project.assignedTickets.map(ticket => {
          if (data.id === ticket.id) {
            data.data.id = ticket.id;
            ticket = this.mapperService.mapJsonToAssignedSingleTickets(data.data);
          }
          return ticket;
        });
        this.project.assignedTickets = this.newAssignedTickets;
        this.filterdTickets = this.newAssignedTickets;
      }
    });

    // Delete selected ticket from assignedTickets array
    this.pubsub.$sub('ticket:deleted', (data) => {
      this.project.assignedTickets = this.project.assignedTickets.filter(ticket => {
        if (ticket.label !== data.item.label) {
          return ticket;
        }
      });

      // Applying changes on filteredTickerts Array to refresh data inside the box.
      this.filterdTickets = this.project.assignedTickets;
    });
  }

  openDialogTickets () {
    const dialogRefTicket = this.dialog.open(TicketsDialogComponent, {
      width: environment.rightDialogs.width,
      height: environment.rightDialogs.height,
      panelClass: 'ticket-add-modal',
      autoFocus: false,
      data: {
        mode: 'add',
        users: this.getProjectUsers(),
        project: this.project,
      },
      hasBackdrop: true
    });

    // TODO: get back here when API is ready.
    dialogRefTicket.afterClosed();
  }

  openDetailsTickets (ticketItem) {
    const dialogRefTicket = this.dialog.open(TicketsDialogComponent, {
      width: environment.rightDialogs.width,
      height: environment.rightDialogs.height,
      panelClass: 'ticket-add-modal',
      autoFocus: false,
      data: {
        mode: 'details',
        project: this.project,
        users: this.getProjectUsers(),
        ticket: ticketItem
      },
    });
    // TODO: get back here when API is ready.
    dialogRefTicket.afterClosed();
  }

  editTicket (ticketItem) {
    const dialogRefTicket = this.dialog.open(TicketsDialogComponent, {
      width: environment.rightDialogs.width,
      height: environment.rightDialogs.height,
      panelClass: 'ticket-add-modal',
      autoFocus: false,
      data: {
        mode: 'edit',
        users: this.getProjectUsers(),
        ticket: ticketItem
      },
    });
    // TODO: get back here when API is ready.
    dialogRefTicket.afterClosed();
  }

  getProjectUsers (): Array<any> {
    if (this.project.assignedUsers && this.users) {
      const users = [];
      this.users.map(user => {
        this.project.assignedUsers.map(assignedUserId => {
          if (assignedUserId === user.id) {
            users.push(user);
          }
        });
      });
      return users;
    }
  }

  getPercentValue (time: Time): number {
    let passedTime = 0;

    time.logged.map(loggedTime => {
      passedTime = passedTime + parseFloat(loggedTime.loggedTime);
    });
    return passedTime;
  }

  getPercentMax (time: Time): number {
    return time.estimated ? parseFloat(time.estimated) : 0;
  }

  isEditMode (): boolean {
    return this.mode && this.mode === 'edit' ? true : false;
  }

  searchTickets (value) {
    if (!value) {
      this.filterdTickets = this.project.assignedTickets;
      this.noItemFound = false;
    }
    this.filterdTickets = this.project.assignedTickets.filter(ticket => {
      if (ticket.label.toLocaleLowerCase().includes(value.trim().toLocaleLowerCase()) ||
        ticket.reference.toLocaleLowerCase().includes(value.trim().toLocaleLowerCase())) {
        return ticket;
      }
      if (this.filterdTickets.length === 0) {
        this.noItemFound = true;
      }
    });
  }

}
