import { Component, Input, Output, EventEmitter, OnChanges } from '@angular/core';
@Component({
  selector: 'app-dual-list-box',
  templateUrl: './dual-list-box.component.html',
  styleUrls: ['./dual-list-box.component.scss'],
})
export class DualListBoxComponent implements OnChanges {
  // Data assigantion from dialog
  @Input() label: string = null;
  @Input() assignationArray: Array<any> = [];
  @Input() initArray: Array<any> = [];
  @Input() isDetailsMode: false;

  noItemFoundOnUnassigned = false;
  noItemFoundOnAssigned = false;

  // Send assignation change event to project dialog
  @Output() change: EventEmitter<Array<any>> = new EventEmitter();

  // Store filtred data
  public filtredInitArray = [];
  public filtredAssignationArray = [];

  constructor () {
  }

  ngOnChanges () {
    this.filtredCopy();
  }

  addToAssignedArray (item) {
    this.assignationArray.push(item);
    const index = this.initArray.indexOf(item);
    if (index > -1) {
      this.initArray.splice(index, 1);
    }
    this.filtredAssignationArray = this.assignationArray;
    this.change.emit(this.assignationArray);
  }

  removeFromAssignedArray (item) {
    this.initArray.push(item);
    const index = this.assignationArray.indexOf(item);
    if (index > -1) {
      this.assignationArray.splice(index, 1);
    }
    this.filtredInitArray = this.filtredInitArray;
    this.change.emit(this.assignationArray);
  }

  filtredCopy () {
    this.filtredInitArray = this.initArray;
    this.filtredAssignationArray = this.assignationArray;
  }

  filtredInit (value: string) {
    if (!value) {
      this.filtredInitArray = this.initArray;
    }
    this.filtredInitArray = this.initArray.filter(item => {
      if (item.firstname && item.lastname) {
        const fullName = `${item.firstname} ${item.lastname}`;
        if (fullName.toLocaleLowerCase().includes(value.trim().toLocaleLowerCase())) {
          return item;
        }
      } else {
        const fullName = `${item.projectName}`;

        if (fullName.toLocaleLowerCase().includes(value.trim().toLocaleLowerCase())) {
          return item;
        }
      }
    });

    if (this.filtredInitArray.length === 0) {
      this.noItemFoundOnUnassigned = true;
    } else {
      this.noItemFoundOnUnassigned = false;
    }
  }

  filtredAssignation (value: string) {
    if (!value) {
      this.filtredAssignationArray = this.assignationArray;
    }
    this.filtredAssignationArray = this.assignationArray.filter(item => {
      if (item.firstname && item.lastname) {
        const fullName = `${item.firstname} ${item.lastname}`;
        if (fullName.toLocaleLowerCase().includes(value.trim().toLocaleLowerCase())) {
          return item;
        }
      } else {
        const fullName = `${item.projectName}`;
        if (fullName.toLocaleLowerCase().includes(value.trim().toLocaleLowerCase())) {
          return item;
        }
      }
    });

    if (this.filtredAssignationArray.length === 0) {
      this.noItemFoundOnAssigned = true;
    } else {
      this.noItemFoundOnAssigned = false;
    }
  }

  getPlaceholder (position: string) {
    if (position === 'left') {
      return `All ${this.label.toLowerCase()}`;
    } else if (position === 'right') {
      return `Assigned ${this.label.toLowerCase()}`;
    }
  }

}
