import { Component, OnInit, Inject, EventEmitter } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { Time } from 'src/modules/shared/models/time';
import { DeleteConfirmDialogComponent } from 'src/modules/shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { environment } from 'src/environments/environment';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UploadOutput, UploadInput, UploadFile, humanizeBytes, UploaderOptions } from 'ngx-uploader';
import { MyErrorStateMatcher } from 'src/modules/shared/toolbox/errorMatcher';
import { PubSubService } from 'angular7-pubsub';
import * as moment from 'moment';
import { Attachment } from 'src/modules/shared/models/attachment';
import { Comment } from 'src/modules/shared/models/comment';
import { LoggedTime } from 'src/modules/shared/models/loggedTime';
import { LogWorkDialogComponent } from 'src/modules/shared/dialogs/log-work-dialog/log-work-dialog.component';
import { MapperService } from 'src/modules/shared/providers/mappers/mapper.service';
import { Update } from 'src/modules/shared/models/update';
import { Status } from 'src/modules/shared/enums/status';
import { HistoryDialogComponent } from '../history-dialog/history-dialog.component';

@Component({
  selector: 'app-tickets-dialog',
  templateUrl: './tickets-dialog.component.html',
  styleUrls: ['./tickets-dialog.component.scss']
})
export class TicketsDialogComponent implements OnInit {
  newAssignedTickets = [];
  newLoggedTime = [];
  constructor (
    public dialogRef: MatDialogRef<TicketsDialogComponent>,
    public dialog: MatDialog,
    public pubsub: PubSubService,
    public mapperService: MapperService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.options = { concurrency: 1, maxUploads: 5 };
    // Local uploading files array
    this.files = [];
    // Input events, used to emit data to ngx-uploader
    this.uploadInput = new EventEmitter<UploadInput>();
    this.humanizeBytes = humanizeBytes;
  }

  // Component variables
  matcher = new MyErrorStateMatcher();
  requestInProgress = false;

  // Payload variable to store DATA
  public payload = {
    status: null,
    label: null,
    reference: null,
    assignedUserId: null,
    description: null,
    comments: [],
    importance: null,
    importanceValue: null,
    attachments: [],
    time: {},
    creationDate: null,
    updates: [],
    type: null
  };

  // Payload helpers
  timing: any = {};
  commentsArray: Array<{}> = [];
  attachmentsArray: Array<{}> = [];
  updatesArray: Array<{}> = [];

  // Upload variables
  hide = true;
  options: UploaderOptions;
  formData: FormData;
  files: UploadFile[];
  uploadInput: EventEmitter<UploadInput>;
  humanizeBytes: (param: number) => string;
  dragOver: boolean;

  // Ticket Form Validation
  public ticketForm = new FormGroup({
    label: new FormControl({ value: '', disabled: this.isDetailsMode() ? true : false }, [Validators.required]),
    reference: new FormControl({ value: '', disabled: true }, [Validators.required]),
    assignedUserId: new FormControl({ value: '', disabled: this.isDetailsMode() ? true : false }, [Validators.required]),
    description: new FormControl({ value: '', disabled: this.isDetailsMode() ? true : false }, [Validators.required]),
    comment: new FormControl({ value: '', disabled: false }),
    attachments: new FormControl({ value: '', disabled: this.isDetailsMode() }),
    estimated: new FormControl({ value: '', disabled: this.isDetailsMode() || this.data.source ? true : false }, [Validators.required]),
    remaining: new FormControl({ value: '', disabled: true }),
    logged: new FormControl({ value: '', disabled: true }),
    status: new FormControl({ value: Status.TODO, disabled: false }),
    importance: new FormControl({ value: 'minimal', disabled: false }),
    type: new FormControl({ value: 'FEAT', disabled: false }),
  });

  ngOnInit () {
  }

  saveForm (type?) {
    // Check form validation before processing DATA
    if (!this.ticketForm.valid) {
      return;
    }

    // Disable all controls
    this.requestInProgress = true;
    for (const control in this.ticketForm.controls) {
      if (this.ticketForm.controls) {
        this.ticketForm.get(control).disable();
      }
    }

    // Add Ticket Mode
    if (type && type === 'add') {

      // Assign Timesheeting to helper variable
      this.timing = {
        estimated: this.ticketForm.get('estimated').value,
        remaining: this.ticketForm.get('estimated').value,
        logged: [],
      };

      // Assign Comment to helper variable
      const comment: Comment = new Comment();
      comment.userId = this.getAssignedUser(this.data);
      comment.comment = this.ticketForm.get('comment').value;
      comment.date = moment(new Date()).format('YYYY-MM-DD');
      // Check if comment exit before pushing in Array
      if (comment.comment) {
        this.commentsArray.push(comment);
      }

      // Assign Attachments to helper variable
      this.files.map(file => {
        const attachement: Attachment = new Attachment();
        attachement.id = file.fileIndex;
        attachement.name = file.name.split('.')[0]; // Set name without extention
        attachement.type = file.type.split('/')[0]; // Set only uploaded file type
        attachement.url = file.name; // Set file name url.

        this.attachmentsArray.push(attachement);
      });

      // Storing DATA in payload
      this.payload = {
        status: Status.TODO,
        label: this.ticketForm.get('label').value,
        importance: this.ticketForm.get('importance').value,
        importanceValue: this.getImportanceValue(this.ticketForm.get('importance').value),
        assignedUserId: this.ticketForm.get('assignedUserId').value,
        reference: this.ticketForm.get('reference').value,
        description: this.ticketForm.get('description').value,
        comments: this.commentsArray,
        attachments: this.attachmentsArray,
        type: this.ticketForm.get('type').value,
        time: this.timing,
        creationDate: moment(new Date()).format('YYYY-MM-DD hh:mm:ss'),
        updates: [{
          id: 1,
          assignedUser: this.ticketForm.get('assignedUserId').value,
          status: Status.TODO,
          editionDate: moment(new Date()).format('YYYY-MM-DD hh:mm:ss'),
          description: 'Ticket creation'
        }]
      };

      this.pubsub.$pub('new:ticket', {
        type,
        data: this.payload,
      });

      this.dialogRef.close();

      // Edit Mode
    } else {
      // Assign Timesheeting to helper variable
      this.timing = {
        estimated: this.ticketForm.get('estimated').value,
        remaining: this.ticketForm.get('remaining').value,
        logged: this.data.ticket.time.logged
      };

      // Assign Comment to helper variable
      const comment: Comment = new Comment();
      comment.userId = this.getAssignedUser(this.data);
      comment.comment = this.ticketForm.get('comment').value;
      comment.date = moment(new Date()).format('YYYY-MM-DD');
      // Check if comment exit before pushing in Array
      if (comment.comment) {
        this.commentsArray.push(comment);
      }

      // Assign Attachments to helper variable
      this.files.map(file => {
        const attachement: Attachment = new Attachment();
        attachement.id = file.fileIndex;
        attachement.name = file.name;
        attachement.type = file.type;
        attachement.url = file.name;

        this.attachmentsArray.push(attachement);
      });

      // Assign updates only if theres changes
      if (this.getChangedAttributeMessage()) {
        const update: Update = new Update();
        update.id = this.data.ticket.updates && this.data.ticket.updates.length + 1;
        update.status = this.ticketForm.get('status').value;
        update.editionDate = moment(new Date()).format('YYYY-MM-DD HH:m:ss');
        update.assignedUserId = this.ticketForm.get('assignedUserId').value;

        update.description = this.getChangedAttributeMessage();
        // Check if comment exit before pushing in Array
        if (update) {
          this.updatesArray.push(update);
        }
      }

      // Storing DATA in payload
      this.payload = {
        status: this.ticketForm.get('status').value,
        label: this.ticketForm.get('label').value,
        assignedUserId: this.ticketForm.get('assignedUserId').value,
        importance: this.ticketForm.get('importance').value,
        type: this.ticketForm.get('type').value,
        importanceValue: this.getImportanceValue(this.ticketForm.get('importance').value),
        reference: this.ticketForm.get('reference').value,
        description: this.ticketForm.get('description').value,
        comments: this.data.ticket.comments ? [...this.data.ticket.comments, ...this.commentsArray] : this.commentsArray,
        attachments: this.data.ticket.attachments ? [...this.data.ticket.attachments, ...this.attachmentsArray] : this.attachmentsArray,
        time: this.timing,
        creationDate: this.data.ticket.creationDate,
        updates: this.data.ticket.updates ? [...this.data.ticket.updates, ...this.updatesArray] : this.updatesArray
      };

      this.pubsub.$pub('new:ticket', {
        type,
        data: this.payload,
        id: this.data.ticket.id
      });
      this.dialogRef.close();
    }
  }

  getChangedAttributeMessage (): string {
    let message = '';
    for (const control in this.ticketForm.controls) {
      if (this.ticketForm.controls) {
        for (const attr in this.data.ticket) {
          if (attr === control) {
            if (this.data.ticket[attr] !== this.ticketForm.get(control).value) {
              if (control === 'description' || control === 'label') {
                message += '<p><b>' + control.toUpperCase() + '</b> was changed.</p>';
              } else if (control !== 'attachments' && control !== 'comment') {
                message += '<p><b>' + control.toUpperCase() + '</b> was changed from ' +
                  this.data.ticket[attr] + ' to ' + this.ticketForm.get(control).value + '</p>';
              }
            }
          }
        }
      }
    }
    if (message) {
      return message;
    } else {
      return null;
    }
  }

  onUploadOutput (output: UploadOutput): void {
    switch (output.type) {
      case 'allAddedToQueue':
        // uncomment this if you want to auto upload files when added
        /*   const event: UploadInput = {
          type: 'uploadAll',
          url: '/upload',
          method: 'POST',
          data: { foo: 'bar' }
        };
        this.uploadInput.emit(event); */
        break;
      case 'addedToQueue':
        if (typeof output.file !== 'undefined') {
          // this.uploadedFile = output.file.name;
          this.files.push(output.file);
        }
        break;
      case 'uploading':
        if (typeof output.file !== 'undefined') {
          // update current data in files array for uploading file
          const index = this.files.findIndex((file) => typeof output.file !== 'undefined' && file.id === output.file.id);
          this.files[index] = output.file;
        }
        break;
      case 'removed':
        // remove file from array when removed
        this.files = this.files.filter((file: UploadFile) => file !== output.file);
        break;
      case 'dragOver':
        this.dragOver = true;
        break;
      case 'dragOut':
      case 'drop':
        this.dragOver = false;
        break;
      case 'done':
        // The file is downloaded
        break;
    }
  }

  startUpload (): void {
    const event: UploadInput = {
      type: 'uploadAll',
      url: 'http://ngx-uploader.com/upload',
      method: 'POST',
      data: { foo: 'bar' }
    };
    this.uploadInput.emit(event);
  }

  cancelUpload (id: string): void {
    this.uploadInput.emit({ type: 'cancel', id });
  }

  removeFile (id: string): void {
    this.uploadInput.emit({ type: 'remove', id });
  }

  removeAllFiles (): void {
    this.uploadInput.emit({ type: 'removeAll' });
  }

  isEditMode (): boolean {
    return this.data && this.data.mode === 'edit' ? true : false;
  }

  isAddMode (): boolean {
    return this.data && this.data.mode === 'add' ? true : false;
  }

  isDetailsMode (): boolean {
    return this.data && this.data.mode === 'details' ? true : false;
  }

  onEditClick () {
    this.data.mode = 'edit';
    for (const control in this.ticketForm.controls) {
      if (this.ticketForm.controls && control !== 'remaining' && control !== 'logged') {
        this.ticketForm.get(control).enable();
      }
    }
  }

  onCancel () {
    this.dialogRef.close();
  }

  getUsers (): Array<any> {
    return this.data && this.data.users ? this.data.users : [];
  }

  getLabel (data) {
    return data.ticket && data.ticket.label ? data.ticket.label : '';
  }

  getTicketStatus (data) {
    return data.ticket && data.ticket.status ? data.ticket.status : Status.TODO;
  }

  getTicketImportance (data) {
    return data.ticket && data.ticket.importance ? data.ticket.importance : 'minimal';
  }

  getTicketType (data) {
    return data.ticket && data.ticket.type ? data.ticket.type : 'FEAT';
  }

  getImportanceValue (importanceLabel): number {
    switch (importanceLabel) {
      case 'minimal':
        return 0;
      case 'average':
        return 1;
      case 'high':
        return 2;
      case 'critical':
        return 3;
    }
  }

  getTicketRef (data) {
    return data.ticket && data.ticket.reference ? data.ticket.reference : '';
  }

  setTicketRef (data) {
    return data && data.project ? `${data.project.projectName}-${this.checkTwoDigits(data.project.assignedTickets.length + 1)}` : '';
  }

  getDescription (data) {
    return data.ticket && data.ticket.description ? data.ticket.description : '';
  }

  getAssignedUser (data) {
    return data.ticket && data.ticket.assignedUserId ? data.ticket.assignedUserId : '';
  }

  getComments (data): Array<any> {
    return data.ticket && data.ticket.comments ? data.ticket.comments : [];
  }

  getAttachment (data): Array<any> {
    return data.ticket && data.ticket.attachments ? data.ticket.attachments : [];
  }

  getTime (data): Time {
    return data.ticket && data.ticket.time ? data.ticket.time : {};
  }

  getRemaining (data): number {
    if (data.ticket && data.ticket.time) {
      const estimated: number = parseFloat(data.ticket.time.estimated);
      const logged = this.calcLogged(data.ticket.time.logged);

      return estimated - logged;
    } else {
      return 0;
    }
  }

  getEstimated (data): number {
    if (data.ticket && data.ticket.time) {
      const estimated: number = parseFloat(data.ticket.time.estimated);
      return estimated;
    } else {
      return 0;
    }
  }

  refreshRemainingTime (event) {
    const value = event.target.value;
    let newValue;

    newValue = value - this.ticketForm.get('logged').value;
    this.ticketForm.get('remaining').setValue(newValue);
  }

  getLoggedTime (data): Array<any> {
    return data.ticket && data.ticket.time.logged ? data.ticket.time.logged : [];
  }

  getUserName (id): string {
    let fullname = null;

    this.getUsers().map(user => {
      if (user.id === id) {
        fullname = user.firstname + ' ' + user.lastname;
      }
    });
    return fullname;
  }

  checkTwoDigits (n) {
    return (n < 10 ? '0' : '') + n;
  }

  displayHistory (item) {
    this.dialog.open(HistoryDialogComponent, {
      minWidth: '250px',
      height: '100%',
      position: { top: '0', right: '0' },
      panelClass: 'ticket-history',
      data: {
        updatesArray: item.ticket.updates,
        users: item.users
      },
    });
  }

  openDeleteConfirmationDialog (item) {
    const deleteTicketDialog = this.dialog.open(DeleteConfirmDialogComponent, {
      width: environment.deleteDialog.width,
      panelClass: 'delete-confirmation',
      data: {
        ticket: item,
      },
    });
    // TODO: get back when API is ready
    deleteTicketDialog.afterClosed().subscribe(data => {
      if (data.isDeleted === true) {
        this.dialogRef.close();
      }
    });
  }

  openLogWorkDialog (item) {
    const logWorkDialog = this.dialog.open(LogWorkDialogComponent, {
      width: environment.logWorkDialog.width,
      panelClass: 'log-work',
      data: {
        ticket: item.ticket,
        project: item.project
      },
      autoFocus: true
    });
    // TODO: get back when API is ready
    logWorkDialog.afterClosed().subscribe(result => {
      if (this.data.ticket.id === result.id) {
        this.newLoggedTime = result.currentLogged;
        this.data.ticket.time.logged.push(this.newLoggedTime);

        // While logging a work , it means automatically that there's an update
        const update = new Update();
        update.id = this.data.ticket.updates.length && this.data.ticket.updates > 0 ? this.data.ticket.updates.length + 1 : 1;
        update.assignedUserId = this.data.ticket.assignedUserId;
        update.status = this.data.ticket.status;
        update.editionDate = moment(new Date()).format('YYYY-MM-DD HH:m:ss');
        result.data.map((logged, index) => {
          if (result.data.length - 1 === index) {
            update.description = '<p><b>Logged : ' + result.currentLogged.loggedTime +
              this.getUnit(result.currentLogged.loggedTime)
              + '</b> - ' + logged.message + '</p>';
          }
        });

        this.data.ticket.updates.push(update);
      }
    });
  }

  getUnit (loggedValue): string {
    return parseFloat(loggedValue) > parseFloat('1') ? ' day(s)' : ' hour(s)';
  }

  calcLogged (loggedTimes: Array<LoggedTime>): number {
    let passedTime = 0;

    if (loggedTimes) {
      loggedTimes.map(loggedTime => {
        if (loggedTime.loggedTime) {
          passedTime = passedTime + parseFloat(loggedTime.loggedTime);
        }
      });
    }
    return passedTime;
  }

  calcRemaining (estimatedTime, loggedTimes) {
    let remainingTime = estimatedTime;
    if (loggedTimes) {
      const logTime = this.calcLogged(loggedTimes);
      remainingTime = estimatedTime - logTime;
    }
    return remainingTime;
  }

}
