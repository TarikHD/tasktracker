import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketsDialogComponent } from './tickets-dialog.component';

describe('AddTicketComponent', () => {
  let component: TicketsDialogComponent;
  let fixture: ComponentFixture<TicketsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TicketsDialogComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
