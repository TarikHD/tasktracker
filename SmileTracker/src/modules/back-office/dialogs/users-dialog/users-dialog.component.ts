import { Component, OnInit, EventEmitter, Inject, Output } from '@angular/core';
import { UploadOutput, UploadInput, UploadFile, humanizeBytes, UploaderOptions } from 'ngx-uploader';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Project } from 'src/modules/shared/models/project';
import { PubSubService } from 'angular7-pubsub';
import { MyErrorStateMatcher } from 'src/modules/shared/toolbox/errorMatcher';
import { UsersService } from 'src/modules/shared/providers/users/users.service';
import { Role } from 'src/modules/shared/enums/role';
import { ProjectsHelperService } from 'src/modules/shared/providers/projectsHelper/projects-helper.service';

@Component({
  selector: 'app-users-dialog',
  templateUrl: './users-dialog.component.html',
  styleUrls: ['./users-dialog.component.scss']
})
export class UsersDialogComponent implements OnInit {
  // DATA stored in payload to be sent to the server
  public payload = {
    firstname: null,
    lastname: null,
    password: null,
    role: null,
    email: null,
    assignedProjects: [],
    image: null
  };

  // Upload variables
  hide = true;
  options: UploaderOptions;
  formData: FormData;
  files: UploadFile[];
  uploadInput: EventEmitter<UploadInput>;
  humanizeBytes: (param: number) => string;
  dragOver: boolean;

  // Component variables
  projects: Array<Project> = [];
  assignedProjects: Array<Project> = [];

  // Component helpers
  uploadedImage: string = null; // when we uplaod an image we fill this variable
  assignedEmitterProjects: Array<Project> = null; // Emitter projects variable

  roles: Role;
  rolesArray: Array<object> = [];

  public userForm = new FormGroup({
    firstname: new FormControl({ value: '', disabled: this.isDetailsMode() ? true : false }, [Validators.required]),
    lastname: new FormControl({ value: '', disabled: this.isDetailsMode() ? true : false }, [Validators.required]),
    password: new FormControl({ value: '', disabled: this.isDetailsMode() ? true : false }, [Validators.required]),
    email: new FormControl({ value: '', disabled: this.isDetailsMode() ? true : false }, [
      Validators.required,
      Validators.email,
      Validators.pattern(
        /^[_a-zéèàêâùïüë0-9-+]+(\.[_a-zéèàêâùïüë0-9-+]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,24})$/
      )
    ]),
    role: new FormControl({ value: '', disabled: this.isDetailsMode() }, [Validators.required]),
    picture: new FormControl({ value: '', disabled: this.isDetailsMode() ? true : false })
  });

  matcher = new MyErrorStateMatcher();
  requestInProgress = false;

  constructor (
    public dialogRef: MatDialogRef<UsersDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public projectsHelperService: ProjectsHelperService,
    private usersService: UsersService,
    private pubsub: PubSubService
  ) {
    this.options = { concurrency: 1, maxUploads: 3 };
    this.files = []; // local uploading files array
    this.uploadInput = new EventEmitter<UploadInput>(); // input events, we use this to emit data to ngx-uploader
    this.humanizeBytes = humanizeBytes;
  }

  ngOnInit () {
    this.initRoles();
    this.getProjects();
  }

  initRoles () {
    // Looping through enum Roles, to fill in array and displaying it as a selectBox
    let i = 0;
    for (const item in Role) {
      if (item) {
        this.rolesArray.push({
          id: i,
          name: item
        });
        i++;
      }
    }
  }

  saveForm (type?) {
    // Testing if form is valid.
    if (!this.userForm.valid) {
      return;
    }

    // - Disable all controls prevent edition while requesting the API.
    this.requestInProgress = true;

    for (const control in this.userForm.controls) {
      if (this.userForm.controls) {
        this.userForm.get(control).enable();
      }
    }
    const roleId = parseInt(this.userForm.get('role').value, 10);
    let roleString = '';

    if (roleId !== undefined || roleId != null) {
      let i = 0;
      // Looping through enum Roles, to get selected Role and sending it in the payload
      for (const item in Role) {
        if (Role) {
          if (i === roleId) {
            roleString = item;
          }
          i++;
        }
      }
    }

    if (type && type === 'add') {
      // Setting assignedEmitterProjects as an empty array on Add Mode,
      // cuz it's null and we use it that way on edit Mode, so we shoul re-init as an empty array .
      // this.assignedEmitterProjects = [];
      this.payload = {
        firstname: this.userForm.get('firstname').value,
        lastname: this.userForm.get('lastname').value,
        password: this.userForm.get('password').value,
        role: roleString,
        email: this.userForm.get('email').value,
        image: this.uploadedImage,
        assignedProjects: this.assignedEmitterProjects ? this.assignedEmitterProjects.map(project => {
          return project.id;
        }) : []
      };
      // TODO: it will work only if Real API is ready.
      this.usersService.addUser(this.payload)
        .then(response => {
          // Emit added user to refresh userList content.
          this.pubsub.$pub('new:user', {
            type,
            data: this.payload
          });

          // Close dialog after add sucess.
          this.dialogRef.close();
        })
        .catch(error => {
          console.log(error);
        }).then(() => {
          this.requestInProgress = false;

          for (const control in this.userForm.controls) {
            if (this.userForm.controls) {
              this.userForm.get(control).enable();
            }
          }
        });
    } else {
      this.payload = {
        firstname: this.userForm.get('firstname').value,
        lastname: this.userForm.get('lastname').value,
        password: this.userForm.get('password').value,
        role: roleString,
        email: this.userForm.get('email').value,
        image: this.uploadedImage ? this.uploadedImage : this.data.user.image,
        assignedProjects: this.assignedEmitterProjects ? this.assignedEmitterProjects.map(project => {
          return project.id;
        }) : []
      };

      // TODO: it will work only if Real API is ready.
      this.usersService.editUser(this.data.user.id, this.payload)
        .then(response => {
          // Emit added user to refresh userList content.
          this.pubsub.$pub('new:user', {
            type,
            data: this.payload,
            id: this.data.user.id
          });

          // Close dialog after add sucess.
          this.dialogRef.close();
        })
        .catch(error => {
          console.log(error);
        }).then(() => {
          this.requestInProgress = false;

          for (const control in this.userForm.controls) {
            if (this.userForm.controls) {
              this.userForm.get(control).enable();
            }
          }
        });
    }
  }
  // get informations

  getProjects () {
    if (this.projectsHelperService.getProjects()) {
      this.projects = this.projectsHelperService.getProjects();

      // For displayUserDetails() Function
      if (this.data && this.data.user) {
        // Filling in assigned projects
        this.projects.map(project => {
          this.data.user.assignedProjects.map(item => {
            if (project.id === item) {
              this.assignedProjects.push(project);
            }
          });

          // Removing assigend projects from initial array
          this.data.user.assignedProjects.map(item => {
            this.projects = this.projects.filter(proj => {
              if (proj.id !== item) {
                return proj;
              }
            });
          });
        });
      } else {
        this.assignedProjects = [];
      }
    }
  }

  // Upload scripts

  onUploadOutput (output: UploadOutput): void {
    switch (output.type) {
      case 'allAddedToQueue':
        // uncomment this if you want to auto upload files when added
        /*   const event: UploadInput = {
          type: 'uploadAll',
          url: '/upload',
          method: 'POST',
          data: { foo: 'bar' }
        };
        this.uploadInput.emit(event); */
        break;
      case 'addedToQueue':
        if (typeof output.file !== 'undefined') {
          this.uploadedImage = output.file.name;
          this.files.push(output.file);
        }
        break;
      case 'uploading':
        if (typeof output.file !== 'undefined') {
          // update current data in files array for uploading file
          const index = this.files.findIndex((file) => typeof output.file !== 'undefined' && file.id === output.file.id);
          this.files[index] = output.file;
        }
        break;
      case 'removed':
        // remove file from array when removed
        this.files = this.files.filter((file: UploadFile) => file !== output.file);
        break;
      case 'dragOver':
        this.dragOver = true;
        break;
      case 'dragOut':
      case 'drop':
        this.dragOver = false;
        break;
      case 'done':
        // The file is downloaded
        break;
    }
  }

  startUpload (): void {
    const event: UploadInput = {
      type: 'uploadAll',
      url: 'http://ngx-uploader.com/upload',
      method: 'POST',
      data: { foo: 'bar' }
    };
    this.uploadInput.emit(event);
  }

  cancelUpload (id: string): void {
    this.uploadInput.emit({ type: 'cancel', id });
  }

  removeFile (id: string): void {
    this.uploadInput.emit({ type: 'remove', id });
  }

  removeAllFiles (): void {
    this.uploadInput.emit({ type: 'removeAll' });
  }

  // Filling in form when we want to show details.
  getFirstName (data) {
    return data && data.user && data.user.firstname ? data.user.firstname : '';
  }

  getLastName (data) {
    return data && data.user && data.user.lastname ? data.user.lastname : '';
  }

  getEmail (data) {
    return data && data.user && data.user.email ? data.user.email : '';
  }

  getPassword (data) {
    return data && data.user && data.user.password ? data.user.password : '';
  }

  getUserType (data): number {
    if (data && data.user) {
      // Auto select user role
      // Looping through enum Roles, to get selected Role and displaying it in the selectBox
      let roleNum = 0;
      let i = 0;

      for (const item in Role) {
        if (item) {
          if (item === data.user.role) {
            roleNum = i;
          }
          i++;
        }
      }

      return roleNum;
    } else {
      return;
    }
  }

  getImage (data) {
    return data && data.user && data.user.image ? data.user.image : '';
  }

  // Helpers

  isEditMode (): boolean {
    return this.data && this.data.mode === 'edit' ? true : false;
  }

  isAddMode (): boolean {
    return this.data && this.data.mode === 'add' ? true : false;
  }

  isDetailsMode (): boolean {
    return this.data && this.data.mode === 'details' ? true : false;
  }

  // Options

  onEditClick () {
    this.data.mode = 'edit';

    // Enable all fields
    for (const control in this.userForm.controls) {
      if (this.userForm.controls) {
        this.userForm.get(control).enable();
      }
    }
  }

  onCancel () {
    this.dialogRef.close();
  }

  // Event Listener for Dualbox
  AssignationChanged (event): Array<Project> {
    this.assignedEmitterProjects = event;
    return this.assignedEmitterProjects;
  }
}
