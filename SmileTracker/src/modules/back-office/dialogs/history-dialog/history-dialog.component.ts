import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Update } from 'src/modules/shared/models/update';
import * as moment from 'moment';

@Component({
  selector: 'app-history-dialog',
  templateUrl: './history-dialog.component.html',
  styleUrls: ['./history-dialog.component.scss']
})
export class HistoryDialogComponent implements OnInit {

  constructor (
    public dialogRef: MatDialogRef<HistoryDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit () {
  }

  getUdatesList (): Array<Update> {
    if (this.data && this.data.updatesArray) {
      return this.data.updatesArray;
    } else {
      return null;
    }
  }

  getUserName (id: number): string {
    let fullname: string = null;
    this.data.users.map(user => {
      if (user.id === id) {
        fullname = user.firstname + ' ' + user.lastname;
      }
    });
    return fullname;
  }

  getdateFrom (date): string {
    return moment(date).fromNow();
  }
}
