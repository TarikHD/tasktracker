import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActiveHistoryDialogComponent } from './active-history-dialog.component';

describe('ActiveHistoryDialogComponent', () => {
  let component: ActiveHistoryDialogComponent;
  let fixture: ComponentFixture<ActiveHistoryDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ActiveHistoryDialogComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActiveHistoryDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
