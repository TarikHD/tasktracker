import { Component, OnInit, Inject, EventEmitter } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UploadOutput, UploadInput, UploadFile, humanizeBytes, UploaderOptions } from 'ngx-uploader';
import * as moment from 'moment';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MyErrorStateMatcher } from 'src/modules/shared/toolbox/errorMatcher';
import { PubSubService } from 'angular7-pubsub';
import { ProjectsService } from 'src/modules/shared/providers/projects/projects.service';
import { User } from 'src/modules/shared/models/user';
import { UsersHelperService } from 'src/modules/shared/providers/usersHelper/users-helper.service';

@Component({
  selector: 'app-projects-dialog',
  templateUrl: './projects-dialog.component.html',
  styleUrls: ['./projects-dialog.component.scss']
})
export class ProjectsDialogComponent implements OnInit {

  constructor (
    public dialogRef: MatDialogRef<ProjectsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private usersHelperService: UsersHelperService,
    private projectsService: ProjectsService,
    private pubsub: PubSubService
  ) {
    this.options = { concurrency: 1, maxUploads: 3 };
    this.files = []; // local uploading files array
    this.uploadInput = new EventEmitter<UploadInput>(); // input events, we use this to emit data to ngx-uploader
    this.humanizeBytes = humanizeBytes;
  }

  // Payload variable to store DATA
  public payload = {
    projectName: null,
    startDate: null,
    endDate: null,
    assignedUsers: [],
    assignedTickets: [],
    image: null
  };

  // Upload variables
  hide = true;
  options: UploaderOptions;
  formData: FormData;
  files: UploadFile[];
  uploadInput: EventEmitter<UploadInput>;
  humanizeBytes: (param: number) => string;
  dragOver: boolean;

  // Component variables
  users: Array<any> = [];
  assignedUsers: Array<any> = [];
  tickets: Array<any>;
  assignedTickets: Array<any> = [];
  assignedEmittedUsers: Array<User> = null;

  // Component helpers
  unassignedUsers: Array<any> = [];
  uploadedImage: string = null; // when we uplaod an image we fill this variable
  minDate: string;

  // Form Validation
  public projectForm = new FormGroup({
    projectName: new FormControl({ value: '', disabled: this.isDetailsMode() ? true : false }, [Validators.required]),
    startDate: new FormControl({ value: '', disabled: this.isDetailsMode() ? true : false }, [Validators.required]),
    endDate: new FormControl({ value: '', disabled: this.isDetailsMode() ? true : false },
      [
        Validators.required,
      ]),
    picture: new FormControl({ value: '', disabled: this.isDetailsMode() ? true : false })
  }, {

    });

  matcher = new MyErrorStateMatcher();
  requestInProgress = false;

  ngOnInit () {
    this.getUsers();
  }

  getMinDate (value) {
    this.minDate = moment(value).format('YYYY-MM-DD');
  }

  getUnassignedArray (): Array<any> {
    return this.unassignedUsers;
  }

  getAssignedArray (): Array<any> {
    return this.assignedUsers;
  }

  getUsers () {
    if (this.usersHelperService.getUsers()) {
      this.users = this.usersHelperService.getUsers();
      this.unassignedUsers = this.users;

      if (this.data && this.data.project) {
        // Filling in assigned users from unassignedUsers Array
        this.unassignedUsers.map(user => {
          this.data.project.assignedUsers.map(item => {
            if (user.id === item) {
              this.assignedUsers.push(user);
            }
          });
        });
        // Removing assigned users from initial array
        this.data.project.assignedUsers.map(item => {
          this.unassignedUsers = this.unassignedUsers.filter(unassignedUser => {
            if (unassignedUser.id !== item) {
              return unassignedUser;
            }
          });
        });
      } else {
        this.assignedUsers = [];
      }
    }
  }

  AssignationChanged (event): Array<User> {
    this.assignedEmittedUsers = event;
    return this.assignedEmittedUsers;
  }

  saveForm (type?) {
    if (!this.projectForm.valid) {
      return;
    }
    // - Disable all controls while requesting the API.
    this.requestInProgress = true;
    for (const control in this.projectForm.controls) {
      if (this.projectForm.controls) {
        this.projectForm.get(control).disable();
      }
    }

    if (type && type === 'add') {
      this.payload = {
        projectName: this.projectForm.get('projectName').value,
        startDate: moment(this.projectForm.get('startDate').value).format('YYYY-MM-DD'),
        endDate: moment(this.projectForm.get('endDate').value).format('YYYY-MM-DD'),
        assignedUsers: this.assignedEmittedUsers ? this.assignedEmittedUsers.map(user => {
          return user.id;
        }) : [],
        assignedTickets: [],
        image: this.uploadedImage ? this.uploadedImage : '',
      };

      // It will work only if Real API is ready.
      this.projectsService.addProject(this.payload)
        .then(response => {
          // Emit added project to refresh projectList.
          this.pubsub.$pub('new:project', {
            type,
            data: this.payload,
          }
          );
          // Close Dialog after adding project
          this.dialogRef.close();
        })
        .catch(error => {
        }).then(() => {
          this.requestInProgress = false;

          for (const control in this.projectForm.controls) {
            if (this.projectForm.controls) {
              this.projectForm.get(control).enable();
            }
          }
        });
    } else {
      this.payload = {
        projectName: this.projectForm.get('projectName').value,
        startDate: moment(this.projectForm.get('startDate').value).format('YYYY-MM-DD'),
        endDate: moment(this.projectForm.get('endDate').value).format('YYYY-MM-DD'),
        image: this.uploadedImage ? this.uploadedImage : this.data.project.image,
        assignedUsers: this.data.project.assignedUsers && !this.assignedEmittedUsers
          ? this.data.project.assignedUsers
          : this.assignedEmittedUsers.map(user => user.id),
        assignedTickets: this.data.project.assignedTickets ? this.data.project.assignedTickets : []
      };

      // TODO: it will work only if Real API is ready.
      this.projectsService.editProject(this.data.project.id, this.payload)
        .then(response => {
          this.pubsub.$pub('new:project', {
            type,
            data: this.payload,
            id: this.data.project.id
          });
          this.dialogRef.close();
        })
        .catch(error => {
          console.log(error);
        }).then(() => {
          this.requestInProgress = false;

          for (const control in this.projectForm.controls) {
            if (this.projectForm.controls) {
              this.projectForm.get(control).enable();
            }
          }
        });
    }
  }

  onUploadOutput (output: UploadOutput): void {
    switch (output.type) {
      case 'allAddedToQueue':
        // uncomment this if you want to auto upload files when added
        /*   const event: UploadInput = {
          type: 'uploadAll',
          url: '/upload',
          method: 'POST',
          data: { foo: 'bar' }
        };
        this.uploadInput.emit(event); */
        break;
      case 'addedToQueue':
        if (typeof output.file !== 'undefined') {
          this.uploadedImage = output.file.name;
          this.files.push(output.file);
        }
        break;
      case 'uploading':
        if (typeof output.file !== 'undefined') {
          // update current data in files array for uploading file
          const index = this.files.findIndex((file) => typeof output.file !== 'undefined' && file.id === output.file.id);
          this.files[index] = output.file;
        }
        break;
      case 'removed':
        // remove file from array when removed
        this.files = this.files.filter((file: UploadFile) => file !== output.file);
        break;
      case 'dragOver':
        this.dragOver = true;
        break;
      case 'dragOut':
      case 'drop':
        this.dragOver = false;
        break;
      case 'done':
        // The file is downloaded
        break;
    }
  }

  startUpload (): void {
    const event: UploadInput = {
      type: 'uploadAll',
      url: 'http://ngx-uploader.com/upload',
      method: 'POST',
      data: { foo: 'bar' }
    };
    this.uploadInput.emit(event);
  }

  cancelUpload (id: string): void {
    this.uploadInput.emit({ type: 'cancel', id });
  }

  removeFile (id: string): void {
    this.uploadInput.emit({ type: 'remove', id });
  }

  removeAllFiles (): void {
    this.uploadInput.emit({ type: 'removeAll' });
  }

  getProjectName (data) {
    return data && data.project && data.project.projectName ? data.project.projectName : '';
  }

  getImage (data) {
    return data && data.project && data.project.image ? data.project.image : '';
  }

  getStartDate (data) {
    return data && data.project && data.project.startDate ? moment(data.project.startDate).format('YYYY-MM-DD') : '';
  }

  getEndDate (data) {
    return data && data.project && data.project.endDate ? moment(data.project.endDate).format('YYYY-MM-DD') : '';
  }

  isEditMode (): boolean {
    return this.data && this.data.mode === 'edit' ? true : false;
  }

  isAddMode (): boolean {
    return this.data && this.data.mode === 'add' ? true : false;
  }

  isDetailsMode (): boolean {
    return this.data && this.data.mode === 'details' ? true : false;
  }

  onEditClick () {
    this.data.mode = 'edit';
    for (const control in this.projectForm.controls) {
      if (this.projectForm.controls) {
        this.projectForm.get(control).enable();
      }
    }
  }

  onCancel () {
    this.dialogRef.close();
  }

  isEndDateValid (): boolean {
    return moment(this.projectForm.get('endDate').value).isAfter(this.projectForm.get('startDate').value);
  }
  public fieldHasError (field: string, error: string): boolean {
    const control = this.projectForm.get(field);
    return (
      !!(control && control.invalid && (control.dirty || control.touched)) &&
      control.errors[error]
    );
  }
}
