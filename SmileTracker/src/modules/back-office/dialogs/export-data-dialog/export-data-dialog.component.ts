import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as moment from 'moment';
import { Project } from 'src/modules/shared/models/project';
import { ProjectsHelperService } from 'src/modules/shared/providers/projectsHelper/projects-helper.service';
import { ExportAsService, ExportAsConfig } from 'ngx-export-as';
import { UsersHelperService } from 'src/modules/shared/providers/usersHelper/users-helper.service';
@Component({
  selector: 'app-export-data-dialog',
  templateUrl: './export-data-dialog.component.html',
  styleUrls: ['./export-data-dialog.component.scss']
})
export class ExportDataDialogComponent implements OnInit {
  isGenerated = false;
  stringHTML = null;
  public projects: Array<Project> = null;
  // Variables that will be used to export UPDATES
  public exportedUpdates: Array<any> = [];
  public updatesArray: Array<any> = [];
  public payload = {
    ticketRef: null,
    update: []
  };

  constructor (
    public exportDialog: MatDialogRef<ExportDataDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public projectsHelperService: ProjectsHelperService,
    public usersHelperService: UsersHelperService,
    private exportAsService: ExportAsService
  ) {
  }

  public exportForm = new FormGroup({
    project: new FormControl('', [Validators.required]),
    startDate: new FormControl('', [Validators.required]),
    endDate: new FormControl('', [Validators.required])
  });

  ngOnInit () {
    this.projects = this.projectsHelperService.getProjects();
  }

  public fieldHasError (field: string, error: string): boolean {
    const control = this.exportForm.get(field);
    return (
      !!(control && control.invalid && (control.dirty || control.touched)) &&
      control.errors[error]
    );
  }

  isEndDateValid (): boolean {
    return moment(this.exportForm.get('endDate').value).isAfter(this.exportForm.get('startDate').value);
  }

  // GET USER FULLNAME TO DISPLAY IT IN GENERATED UPDATES
  getUserNameById (id: number): string {
    let fullName = null;
    const users = this.usersHelperService.getUsers();
    users.map(user => {
      if (user.id === id) {
        fullName = `${user.firstname} ${user.lastname}`;
      }
    });
    return fullName;

  }
  // GENERATE SELECTED UPDATES TO BE EXPORTED
  generate () {
    this.stringHTML = null;
    this.exportedUpdates = [];

    if (!this.exportForm.valid) {
      return;
    }

    const selectedProject = this.exportForm.get('project').value;
    const from = moment(this.exportForm.get('startDate').value).format('YYYY-MM-DD HH:mm:ss');
    const to = moment(this.exportForm.get('endDate').value).format('YYYY-MM-DD HH:mm:ss');

    this.projectsHelperService.getProjects().map(project => {
      if (project.projectName === selectedProject) {
        project.assignedTickets.map(ticket => {
          this.updatesArray = [];
          ticket.updates.map(update => {
            if (moment(update.editionDate).isBetween(from, to)) {
              this.updatesArray.push(update);
            }
          });
          if (this.updatesArray.length !== 0) {
            this.payload = {
              ticketRef: ticket.reference,
              update: this.updatesArray
            };
            this.exportedUpdates.push(this.payload);
          }
        });
      }
    });

    // FORMATTING JSON INTO A READABLE HTML IN VIEW TO EXPORT IT AS PDF
    this.stringHTML = '<table id="contentToConvert" class="ticketItem flex flex--1 flex-column">';
    this.exportedUpdates.map(item => {
      // Ticket Block
      this.stringHTML += '<tr class="ticketItem_ref flex--justify-center flex--align-center">' +
        '<td>' +
        item.ticketRef +
        '</td>' +
        '</tr>' +
        '<tr class="ticketItem_header flex flex--space-between">' +
        '<th class="ticketItem_header_item userId">User Id</th>' +
        '<th class="ticketItem_header_item description">Description</th>' +
        '<th class="ticketItem_header_item editionDate">Edition Date</th>' +
        '<th class="ticketItem_header_item status">Status</th>' +
        '</tr>';

      // Ticket Updates
      item.update.map(update => {
        this.stringHTML += '<tr class="ticketItem_updates flex flex--space-between">' +
          '<td class="ticketItem_updates_item userId">' + this.getUserNameById(update.assignedUserId) + '</td>' +
          '<td class="ticketItem_updates_item description">' + update.description + '</td>' +
          '<td class="ticketItem_updates_item editionDate">' + update.editionDate + '</td>' +
          '<td class="ticketItem_updates_item status">' + update.status + '</td>' +
          '</tr>';
      });
      // Ticket Updates
    });
    this.stringHTML += '</table>';
    // Ticket Block

    this.isGenerated = true;
  }

  saveExportForm () {

    const exportAsConfig: ExportAsConfig = {
      type: 'xlsx', // the type you want to download
      elementId: 'contentToConvert', // the id of html/table element,
      options: { // html-docx-js document options
        orientation: 'portrait',
        margins: {
          top: '20'
        }
      }
    };
    // download the file using old school javascript method
    this.exportAsService.save(exportAsConfig, 'myHistory-' + moment(new Date()).format('YYYY-MM-DD'));
  }

}
