import { Component, OnInit } from '@angular/core';
import { PubSubService } from 'angular7-pubsub';
import { MapperService } from 'src/modules/shared/providers/mappers/mapper.service';
import { Project } from 'src/modules/shared/models/project';
import { ProjectsHelperService } from 'src/modules/shared/providers/projectsHelper/projects-helper.service';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss']
})
export class ProjectListComponent implements OnInit {
  data = {
    rows: [],
    columns: []
  };

  title: string = null;
  rows = [];
  columns = [];

  genericError: string = null;

  constructor (
    private pubsub: PubSubService,
    private mapperService: MapperService,
    public projectsHelperService: ProjectsHelperService
  ) { }

  ngOnInit () {
    this.getProjects();
    this.loadSubscribers();
    this.fillData();
  }

  loadSubscribers () {
    this.pubsub.$sub('new:project', (data) => {
      if (data) {
        if (data.type === 'add') {
          // data.data.id = this.rows.length + 1;
          const proj = this.mapperService.mapJsonToSingleProject(data.data);
          this.rows.push(proj);

          this.fillTable(this.rows);
        } else {
          const project: Project = new Project();

          project.id = data && data.id && data.id ? data.id : null;
          project.projectName = data && data.data && data.data.projectName ? data.data.projectName : null;
          project.startDate = data && data.data && data.data.startDate ? data.data.startDate : null;
          project.endDate = data && data.data && data.data.endDate ? data.data.endDate : null;
          project.image = data && data.data && data.data.image ? data.data.image : null;
          project.assignedUsers = data && data.data && data.data.assignedUsers ? data.data.assignedUsers : [];
          project.assignedTickets = data && data.data && data.data.assignedTickets ? data.data.assignedTickets : [];

          this.rows.map(row => {
            if (row.id === project.id) {
              row.id = project.id;
              row.projectName = project.projectName;
              row.startDate = project.startDate;
              row.endDate = project.endDate;
              row.image = project.image;
              row.assignedUsers = project.assignedUsers;
              row.assignedTickets = project.assignedTickets;
            }
          });

          this.fillTable(this.rows);
        }
      }
    });

    this.pubsub.$sub('project:deleted', (project) => {
      if (project) {
        this.rows = this.rows.filter(row => {
          if (row.id !== project.item.id) {
            return row;
          }
        });

        this.fillTable(this.rows);
      }
    });
  }

  getProjects () {
    this.projectsHelperService.projectsEventEmitter.subscribe(data => {
      this.fillData();
    });
  }

  fillData () {
    if (this.projectsHelperService.getProjects()) {
      this.fillTable(this.projectsHelperService.getProjects());
    }
  }

  fillTable (projectsArray) {
    // Clearing arrays before assigning new content.
    this.data.rows = [];
    this.data.columns = [];
    this.rows = [];

    this.rows = projectsArray;
    this.columns = [
      { name: 'name' }
    ];
    this.title = 'Projects';
    this.data.rows = this.rows;
    this.data.columns = this.columns;

    // Publishing event to refresh table content.
    this.pubsub.$pub('refresh:table');
  }
}
