import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  barChartData = [
    {
      name: 'Orange',
      value: 17
    },
    {
      name: 'Oncf',
      value: 12
    },
    {
      name: 'Ocp',
      value: 10
    }
  ];

  pieChartData = [
    {
      name: 'Orange',
      value: 17
    },
    {
      name: 'Ocp',
      value: 10
    },
    {
      name: 'Oncf',
      value: 12
    }
  ];
  pieChartScheme = {
    domain: ['#30E07B', '#E75691', '#00C0FF', '#5D6A75']
  };

  constructor () { }

  ngOnInit () {
  }

  onSelect (event) {
    console.log(event);
  }
}
