import { Component, OnInit } from '@angular/core';
import { PubSubService } from 'angular7-pubsub';
import { MapperService } from 'src/modules/shared/providers/mappers/mapper.service';
import { User } from 'src/modules/shared/models/user';
import { UsersHelperService } from 'src/modules/shared/providers/usersHelper/users-helper.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  title: string = null;
  data = {
    rows: [],
    columns: []
  };

  rows = [];
  columns = [];
  genericError: string = null;

  constructor (
    private usersHelperService: UsersHelperService,
    private pubsub: PubSubService,
    private mapperService: MapperService
  ) {

  }

  ngOnInit () {
    this.getUsers();
    this.loadSubscribers();
    this.fillData();
  }

  loadSubscribers () {
    this.pubsub.$sub('new:user', (data) => {
      if (data) {
        if (data.type === 'add') {
          data.data.id = this.rows.length + 1;
          const usr = this.mapperService.mapJsonToSingleUser(data.data);
          this.rows.push(usr);
          this.fillTable(this.rows);
        } else {
          // If edit Mode, mapping the new obtained user.
          const user: User = new User();
          user.assignedProjects = data && data.data && data.data.assignedProjects ? data.data.assignedProjects : [];
          user.email = data && data.data && data.data.email ? data.data.email : null;
          user.firstname = data && data.data && data.data.firstname ? data.data.firstname : null;
          user.id = data && data.data && data.id ? data.id : null;
          user.lastname = data && data.data && data.data.lastname ? data.data.lastname : null;
          user.role = data && data.data && data.data.role ? data.data.role : null;
          user.image = data && data.data && data.data.image ? data.data.image : null;
          user.password = data && data.data && data.data.password ? data.data.password : null;

          this.rows.map(row => {
            if (row.id === user.id) {
              row.id = user.id;
              row.email = user.email;
              row.firstname = user.firstname;
              row.assignedProjects = user.assignedProjects;
              row.lastname = user.lastname;
              row.role = user.role;
              row.image = user.image;
              row.password = user.password;
            }
          });

          this.fillTable(this.rows);
        }
      }
    });

    this.pubsub.$sub('user:deleted', (user) => {
      if (user) {
        this.rows = this.rows.filter(row => {
          if (row.id !== user.item.id) {
            return row;
          }
        });
        this.fillTable(this.rows);
      }
    });
  }

  getUsers () {
    this.usersHelperService.usersEventEmitter.subscribe(data => {
      this.fillData();
    });
  }

  fillData () {
    if (this.usersHelperService.getUsers()) {
      this.fillTable(this.usersHelperService.getUsers());
    }
  }

  // Fill Table Content component after each refresh ( add/edit/delete )
  fillTable (userArray) {
    // Clear all Arrays
    this.data.rows = [];
    this.data.columns = [];
    this.rows = [];

    this.rows = userArray;
    this.columns = [
      { name: 'name' }
    ];
    this.title = 'Users';
    this.data.rows = this.rows;
    this.data.columns = this.columns;

    // Refresh table content.
    this.pubsub.$pub('refresh:table');
  }

}
