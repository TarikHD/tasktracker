import { Comment } from './comment';
import { Attachment } from './attachment';
import { Time } from './time';
import * as moment from 'moment';
import { Update } from './update';

export class Ticket {
  private _id: number = null;
  private _label: string = null;
  private _assignedUserId: number = null;
  private _reference: string = null;
  private _description: string = null;
  private _comments: Array<Comment> = [];
  private _attachments: Array<Attachment> = [];
  private _time: Time = null;
  private _status: string = null;
  private _creationDate: moment.Moment = null;
  private _updates: Array<Update> = [];
  private _importance: string = null;
  private _importanceValue: number = null;
  private _type: string = null;

  /**
   * Getter type
   * @return {string }
   */
  public get type (): string {
    return this._type;
  }

  /**
  * Setter iype
  * @param {string } value
  */
  public set type (value: string) {
    this._type = value;
  }

  /**
   * Getter importanceValue
   * @return {number }
   */
  public get importanceValue (): number {
    return this._importanceValue;
  }

  /**
   * Setter importanceValue
   * @param {number } value
   */
  public set importanceValue (value: number) {
    this._importanceValue = value;
  }

  /**
   * Getter importance
   * @return {string }
   */
  public get importance (): string {
    return this._importance;
  }

  /**
   * Setter importance
   * @param {string } value
   */
  public set importance (value: string) {
    this._importance = value;
  }

  /**
   * Getter time
   * @return {Time }
   */
  public get time (): Time {
    return this._time;
  }

  /**
   * Setter time
   * @param {Time } value
   */
  public set time (value: Time) {
    this._time = value;
  }

  /**
   * Getter creationDate
   * @return {moment.Moment }
   */
  public get creationDate (): moment.Moment {
    return this._creationDate;
  }

  /**
   * Setter creationDate
   * @param {moment.Moment } value
   */
  public set creationDate (value: moment.Moment) {
    this._creationDate = value;
  }

  /**
   * Getter updates
   * @return {Array<Update> }
   */
  public get updates (): Array<Update> {
    return this._updates;
  }

  /**
   * Setter updates
   * @param {Array<Update> } value
   */
  public set updates (value: Array<Update>) {
    this._updates = value;
  }

  /**
  * Getter id
  * @return {number }
  */
  public get id (): number {
    return this._id;
  }

  /**
  * Getter label
  * @return {string }
  */
  public get label (): string {
    return this._label;
  }

  /**
  * Getter assignedUserId
  * @return {number }
  */
  public get assignedUserId (): number {
    return this._assignedUserId;
  }

  /**
  * Getter comments
  * @return {Array<Comment> }
  */
  public get comments (): Array<Comment> {
    return this._comments;
  }

  /**
  * Getter reference
  * @return {string }
  */
  public get reference (): string {
    return this._reference;
  }

  /**
  * Getter description
  * @return {string }
  */
  public get description (): string {
    return this._description;
  }

  /**
  * Getter attachments
  * @return {Array<Attachment> }
  */
  public get attachments (): Array<Attachment> {
    return this._attachments;
  }

  /**
  * Setter id
  * @param {number } value
  */
  public set id (value: number) {
    this._id = value;
  }

  /**
  * Setter label
  * @param {string } value
  */
  public set label (value: string) {
    this._label = value;
  }

  /**
  * Setter assignedUserId
  * @param {number } value
  */
  public set assignedUserId (value: number) {
    this._assignedUserId = value;
  }

  /**
  * Setter comments
  * @param {Array<Comment> } value
  */
  public set comments (value: Array<Comment>) {
    this._comments = value;
  }

  /**
  * Setter reference
  * @param {string } value
  */
  public set reference (value: string) {
    this._reference = value;
  }

  /**
  * Setter description
  * @param {string } value
  */
  public set description (value: string) {
    this._description = value;
  }

  /**
  * Setter attachments
  * @param {Array<attachments> } value
  */
  public set attachments (value: Array<Attachment>) {
    this._attachments = value;
  }

  /**
  * Getter time
  * @return {string }
  */
  public get status (): string {
    return this._status;
  }

  /**
  * Setter time
  * @param {string } value
  */
  public set status (value: string) {
    this._status = value;
  }
}
