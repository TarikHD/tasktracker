export class LoggedTime {
  private _id: number = null;
  private _userId: number = null;
  private _loggedTime: string = null;
  private _message: string = null;

  /**
   * Getter message
   * @return {string }
   */
  public get message (): string {
    return this._message;
  }

  /**
   * Setter message
   * @param {string } value
   */
  public set message (value: string) {
    this._message = value;
  }

  /**
   * Getter id
   * @return {number }
   */
  public get id (): number {
    return this._id;
  }

  /**
   * Getter userId
   * @return {number }
   */
  public get userId (): number {
    return this._userId;
  }

  /**
   * Getter loggedTime
   * @return {string }
   */
  public get loggedTime (): string {
    return this._loggedTime;
  }

  /**
   * Setter id
   * @param {number } value
   */
  public set id (value: number) {
    this._id = value;
  }

  /**
   * Setter userId
   * @param {number } value
   */
  public set userId (value: number) {
    this._userId = value;
  }

  /**
   * Setter loggedTime
   * @param {string } value
   */
  public set loggedTime (value: string) {
    this._loggedTime = value;
  }

}
