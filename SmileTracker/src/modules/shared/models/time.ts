import { LoggedTime } from './loggedTime';

export class Time {
  private _estimated: string = null;
  private _remaining: string = null;
  private _logged: Array<LoggedTime> = [];

  /**
   * Getter estimated
   * @return {string }
   */
  public get estimated (): string {
    return this._estimated;
  }

  /**
   * Getter remaining
   * @return {string }
   */
  public get remaining (): string {
    return this._remaining;
  }

  /**
   * Getter logged
   * @return {Array<LoggedTime> }
   */
  public get logged (): Array<LoggedTime> {
    return this._logged;
  }

  /**
   * Setter estimated
   * @param {string } value
   */
  public set estimated (value: string) {
    this._estimated = value;
  }

  /**
   * Setter remaining
   * @param {string } value
   */
  public set remaining (value: string) {
    this._remaining = value;
  }

  /**
   * Setter logged
   * @param {Array<LoggedTime> } value
   */
  public set logged (value: Array<LoggedTime>) {
    this._logged = value;
  }

}
