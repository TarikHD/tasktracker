import * as moment from 'moment';
import { Ticket } from './ticket';

export class Project {
  private _id: number = null;
  private _projectName: string = null;
  private _startDate: moment.Moment = null;
  private _endDate: moment.Moment = null;
  private _assignedUsers: Array<number> = [];
  private _assignedTickets: Array<Ticket> = [];
  private _image: string = null;

  /**
   * Getter id
   * @return {number }
   */
  public get id (): number {
    return this._id;
  }

  /**
   * Getter projectName
   * @return {string }
   */
  public get projectName (): string {
    return this._projectName;
  }

  /**
   * Getter startDate
   * @return {moment.Moment }
   */
  public get startDate (): moment.Moment {
    return this._startDate;
  }

  /**
   * Getter endDate
   * @return {moment.Moment }
   */
  public get endDate (): moment.Moment {
    return this._endDate;
  }

  /**
   * Getter assignedUsers
   * @return {Array<number> }
   */
  public get assignedUsers (): Array<number> {
    return this._assignedUsers;
  }

  /**
   * Getter assignedTickets
   * @return {Array<Ticket> }
   */
  public get assignedTickets (): Array<Ticket> {
    return this._assignedTickets;
  }

  /**
   * Getter image
   * @return {string }
   */
  public get image (): string {
    return this._image;
  }

  /**
   * Setter id
   * @param {number } value
   */
  public set id (value: number) {
    this._id = value;
  }

  /**
   * Setter projectName
   * @param {string } value
   */
  public set projectName (value: string) {
    this._projectName = value;
  }

  /**
   * Setter startDate
   * @param {moment.Moment } value
   */
  public set startDate (value: moment.Moment) {
    this._startDate = value;
  }

  /**
   * Setter endDate
   * @param {moment.Moment } value
   */
  public set endDate (value: moment.Moment) {
    this._endDate = value;
  }

  /**
   * Setter assignedUsers
   * @param {Array<number> } value
   */
  public set assignedUsers (value: Array<number>) {
    this._assignedUsers = value;
  }

  /**
   * Setter assignedTickets
   * @param {Array<Ticket> } value
   */
  public set assignedTickets (value: Array<Ticket>) {
    this._assignedTickets = value;
  }

  /**
   * Setter image
   * @param {string } value
   */
  public set image (value: string) {
    this._image = value;
  }

}
