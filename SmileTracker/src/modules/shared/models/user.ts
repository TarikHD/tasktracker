export class User {
  private _id: number = null;
  private _firstname: string = null;
  private _lastname: string = null;
  private _password: string = null;
  private _role: string = null;
  private _email: string = null;
  private _assignedProjects: Array<number> = [];
  private _image: string = null;

  /**
   * Getter id
   * @return {number }
   */
  public get id (): number {
    return this._id;
  }

  /**
   * Getter firstname
   * @return {string }
   */
  public get firstname (): string {
    return this._firstname;
  }

  /**
   * Getter lastname
   * @return {string }
   */
  public get lastname (): string {
    return this._lastname;
  }

  /**
   * Getter password
   * @return {string }
   */
  public get password (): string {
    return this._password;
  }

  /**
   * Getter role
   * @return {string }
   */
  public get role (): string {
    return this._role;
  }

  /**
   * Getter email
   * @return {string }
   */
  public get email (): string {
    return this._email;
  }

  /**
   * Getter assignedProjects
   * @return {Array<number> }
   */
  public get assignedProjects (): Array<number> {
    return this._assignedProjects;
  }

  /**
   * Getter image
   * @return {string }
   */
  public get image (): string {
    return this._image;
  }

  /**
   * Setter id
   * @param {number } value
   */
  public set id (value: number) {
    this._id = value;
  }

  /**
   * Setter firstname
   * @param {string } value
   */
  public set firstname (value: string) {
    this._firstname = value;
  }

  /**
   * Setter lastname
   * @param {string } value
   */
  public set lastname (value: string) {
    this._lastname = value;
  }

  /**
   * Setter password
   * @param {string } value
   */
  public set password (value: string) {
    this._password = value;
  }

  /**
   * Setter role
   * @param {string } value
   */
  public set role (value: string) {
    this._role = value;
  }

  /**
   * Setter email
   * @param {string } value
   */
  public set email (value: string) {
    this._email = value;
  }

  /**
   * Setter assignedProjects
   * @param {Array<number> } value
   */
  public set assignedProjects (value: Array<number>) {
    this._assignedProjects = value;
  }

  /**
   * Setter image
   * @param {string } value
   */
  public set image (value: string) {
    this._image = value;
  }

}
