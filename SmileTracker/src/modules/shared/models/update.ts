import * as moment from 'moment';

export class Update {
  private _id: number = null;
  private _assignedUserId: number = null;
  private _status: string = null;
  private _editionDate: string = null;
  private _description: string = null;

  /**
   * Getter description
   * @return {string }
   */
  public get description (): string {
    return this._description;
  }

  /**
   * Setter description
   * @param {string } value
   */
  public set description (value: string) {
    this._description = value;
  }

  /**
   * Getter id
   * @return {number }
   */
  public get id (): number {
    return this._id;
  }

  /**
   * Setter id
   * @param {number } value
   */
  public set id (value: number) {
    this._id = value;
  }

  /**
   * Getter assignedUserId
   * @return {number }
   */
  public get assignedUserId (): number {
    return this._assignedUserId;
  }

  /**
   * Setter assignedUserId
   * @param {number } value
   */
  public set assignedUserId (value: number) {
    this._assignedUserId = value;
  }

  /**
   * Getter status
   * @return {string }
   */
  public get status (): string {
    return this._status;
  }

  /**
   * Setter status
   * @param {string } value
   */
  public set status (value: string) {
    this._status = value;
  }

  /**
   * Getter editionDate
   * @return {string }
   */
  public get editionDate (): string {
    return this._editionDate;
  }

  /**
   * Setter editionDate
   * @param {string} value
   */
  public set editionDate (value: string) {
    this._editionDate = value;
  }

}
