import * as moment from 'moment';

export class Comment {
  private _id: number = null;
  private _userId: number = null;
  private _comment: string = null;
  private _date: string = null;

  /**
   * Getter id
   * @return {number }
   */
  public get id (): number {
    return this._id;
  }

  /**
   * Getter userId
   * @return {number }
   */
  public get userId (): number {
    return this._userId;
  }

  /**
   * Getter comment
   * @return {string }
   */
  public get comment (): string {
    return this._comment;
  }

  /**
   * Getter date
   * @return {string }
   */
  public get date (): string {
    return this._date;
  }

  /**
   * Setter id
   * @param {number } value
   */
  public set id (value: number) {
    this._id = value;
  }

  /**
   * Setter userId
   * @param {number } value
   */
  public set userId (value: number) {
    this._userId = value;
  }

  /**
   * Setter comment
   * @param {string } value
   */
  public set comment (value: string) {
    this._comment = value;
  }

  /**
   * Setter date
   * @param {string } value
   */
  public set date (value: string) {
    this._date = value;
  }

}
