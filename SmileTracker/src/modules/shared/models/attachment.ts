export class Attachment {
  private _id: number = null;
  private _name: string = null;
  private _type: string = null;
  private _url: string = null;

  /**
   * Getter name
   * @return {string }
   */
  public get name (): string {
    return this._name;
  }

  /**
   * Setter name
   * @param {string } value
   */
  public set name (value: string) {
    this._name = value;
  }

  /**
  * Getter id
  * @return {number }
  */
  public get id (): number {
    return this._id;
  }

  /**
  * Getter type
  * @return {string }
  */
  public get type (): string {
    return this._type;
  }

  /**
  * Getter url
  * @return {string }
  */
  public get url (): string {
    return this._url;
  }

  /**
  * Setter id
  * @param {number } value
  */
  public set id (value: number) {
    this._id = value;
  }

  /**
  * Setter type
  * @param {string } value
  */
  public set type (value: string) {
    this._type = value;
  }

  /**
  * Setter url
  * @param {string } value
  */
  public set url (value: string) {
    this._url = value;
  }

}
