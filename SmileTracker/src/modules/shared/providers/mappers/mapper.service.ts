import { Injectable } from '@angular/core';
import { Project } from '../../models/project';
import { Ticket } from '../../models/ticket';
import { Time } from '../../models/time';
import { Comment } from '../../models/comment';
import { LoggedTime } from '../../models/loggedTime';
import { User } from '../../models/user';
import { Attachment } from '../../models/attachment';
import { Update } from '../../models/update';

@Injectable({
  providedIn: 'root'
})
export class MapperService {

  constructor () { }

  // Mapping Projects
  public mapJsonToProjects (projectsArray): Array<Project> {
    const projects: Array<Project> = [];

    // Looping throug the obtained array from Backend
    projectsArray.map(project => projects.push(this.mapJsonToSingleProject(project)));

    return projects;
  }

  public mapJsonToSingleProject (projectElement): Project {
    const project: Project = new Project();

    project.id = projectElement.id ? projectElement.id : null;
    project.projectName = projectElement.projectName ? projectElement.projectName : null;
    project.startDate = projectElement.startDate ? projectElement.startDate : null;
    project.endDate = projectElement.endDate ? projectElement.endDate : null;
    project.image = projectElement.image ? projectElement.image : null;
    project.assignedUsers = projectElement.assignedUsers ? projectElement.assignedUsers : [];
    project.assignedTickets = projectElement.assignedUsers ? this.mapJsonToAssignedTickets(projectElement.assignedTickets) : [];

    return project;
  }

  public mapJsonToAssignedTickets (assignedTicketsArray): Array<Ticket> {
    const ticketsArray: Array<Ticket> = [];

    // Looping throug the obtained array from Backend
    assignedTicketsArray.map(ticket => ticketsArray.push(this.mapJsonToAssignedSingleTickets(ticket)));

    return ticketsArray;
  }

  public mapJsonToAssignedSingleTickets (assignedSingleTickets): Ticket {
    const ticket: Ticket = new Ticket();
    ticket.id = assignedSingleTickets.id ? assignedSingleTickets.id : null;
    ticket.label = assignedSingleTickets.label ? assignedSingleTickets.label : null;
    ticket.reference = assignedSingleTickets.reference ? assignedSingleTickets.reference : null;
    ticket.description = assignedSingleTickets.description ? assignedSingleTickets.description : null;
    ticket.assignedUserId = assignedSingleTickets.assignedUserId ? assignedSingleTickets.assignedUserId : null;
    ticket.comments = assignedSingleTickets.comments ? this.mapJsonToComments(assignedSingleTickets.comments) : [];
    ticket.attachments = assignedSingleTickets.attachments ? this.mapJsonToAttachments(assignedSingleTickets.attachments) : [];
    ticket.time = assignedSingleTickets.time ? this.mapJsonToTime(assignedSingleTickets.time) : null;
    ticket.status = assignedSingleTickets.status ? assignedSingleTickets.status : null;
    ticket.creationDate = assignedSingleTickets.creationDate ? assignedSingleTickets.creationDate : null;
    ticket.updates = assignedSingleTickets.updates ? this.mapJsonToUpdates(assignedSingleTickets.updates) : null;
    ticket.importance = assignedSingleTickets.importance ? assignedSingleTickets.importance : null;
    ticket.type = assignedSingleTickets.type ? assignedSingleTickets.type : 'FEAT';
    ticket.importanceValue = assignedSingleTickets.importanceValue ? assignedSingleTickets.importanceValue : null;

    return ticket;
  }

  public mapJsonToUpdates (updatesArray): Array<Update> {
    const updates: Array<Update> = [];

    // Looping throug the obtained array from Backend
    updatesArray.map(update => updates.push(this.mapJsonToSingleUpdate(update)));

    return updates;
  }

  public mapJsonToSingleUpdate (singleUpdate): Update {
    const update: Update = new Update();

    update.id = singleUpdate.id ? singleUpdate.id : null;
    update.status = singleUpdate.status ? singleUpdate.status : null;
    update.editionDate = singleUpdate.editionDate ? singleUpdate.editionDate : null;
    update.assignedUserId = singleUpdate.assignedUserId ? singleUpdate.assignedUserId : null;
    update.description = singleUpdate.description ? singleUpdate.description : null;
    return update;
  }

  public mapJsonToComments (commentsArray): Array<Comment> {
    const comments: Array<Comment> = [];

    // Looping throug the obtained array from Backend
    commentsArray.map(comment => comments.push(this.mapJsonToSingleComment(comment)));

    return comments;
  }

  public mapJsonToSingleComment (singleComment): Comment {
    const comment: Comment = new Comment();

    comment.id = singleComment.id ? singleComment.id : null;
    comment.userId = singleComment.userId ? singleComment.userId : null;
    comment.date = singleComment.date ? singleComment.date : null;
    comment.comment = singleComment.comment ? singleComment.comment : null;

    return comment;
  }

  public mapJsonToAttachments (attachmentArray): Array<Attachment> {
    const attachments: Array<Attachment> = [];

    attachmentArray.map(attachement => attachments.push(this.mapJsonToSingleAttachment(attachement)));

    return attachments;
  }

  public mapJsonToSingleAttachment (singleAttachment): Attachment {
    const attachement: Attachment = new Attachment();

    attachement.id = singleAttachment.id ? singleAttachment.id : null;
    attachement.name = singleAttachment.name ? singleAttachment.name : null;
    attachement.type = singleAttachment.type ? singleAttachment.type : null;
    attachement.url = singleAttachment.url ? singleAttachment.url : null;

    return attachement;
  }

  public mapJsonToTime (timeJson): Time {
    const time: Time = new Time();

    time.estimated = timeJson.estimated ? timeJson.estimated : null;
    time.logged = timeJson.logged ? this.mapJsonToLoggedTime(timeJson.logged) : null;
    time.remaining = timeJson.remaining ? timeJson.remaining : null;

    return time;
  }

  public mapJsonToLoggedTime (loggedArray): Array<LoggedTime> {
    const loggedTime: Array<LoggedTime> = [];

    // Looping throug the obtained array from Backend
    loggedArray.map(loggedItem => loggedTime.push(this.mapJsonToSingleLoggedTime(loggedItem)));

    return loggedTime;
  }

  public mapJsonToSingleLoggedTime (loggedTimeJson): LoggedTime {
    const loggedTime: LoggedTime = new LoggedTime();
    loggedTime.id = loggedTimeJson.id ? loggedTimeJson.id : null;
    loggedTime.loggedTime = loggedTimeJson.loggedTime ? loggedTimeJson.loggedTime : null;
    loggedTime.userId = loggedTimeJson.userId ? loggedTimeJson.userId : null;
    loggedTime.message = loggedTimeJson.message ? loggedTimeJson.message : null;

    return loggedTime;
  }

  // Mapping Users
  public mapJsonToUsers (usersArray): Array<User> {
    const users: Array<User> = [];

    usersArray.map(user => users.push(this.mapJsonToSingleUser(user)));

    return users;
  }

  public mapJsonToSingleUser (userElement): User {
    const user: User = new User();

    user.id = userElement.id ? userElement.id : null;
    user.firstname = userElement.firstname ? userElement.firstname : null;
    user.lastname = userElement.lastname ? userElement.lastname : null;
    user.password = userElement.password ? userElement.password : null;
    user.email = userElement.email ? userElement.email : null;
    user.role = userElement.role ? userElement.role : null;
    user.image = userElement.image ? userElement.image : null;
    user.assignedProjects = userElement.assignedProjects ? userElement.assignedProjects : [];

    return user;
  }

}
