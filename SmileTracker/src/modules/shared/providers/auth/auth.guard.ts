// src/app/auth/auth-guard.service.ts
import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { AuthService } from './auth.service';
import { Role } from '../../enums/role';

@Injectable()
export class AuthGuardService implements CanActivate {

  constructor (
    public auth: AuthService,
    public router: Router
  ) {
  }
  canActivate (route: ActivatedRouteSnapshot): boolean {
    const authorizedRoles = route.data.roles;
    const user = JSON.parse(localStorage.getItem('currentUser'));

    // - Is the user authenticated? (or is this an anonymous route?)
    if (!user) {
      if (authorizedRoles.includes(Role.ANONYMOUS)) {
        // - No further check needed: anonymous can access this route.
        return true;
      }

      this.router.navigate(['login']);
      return false;
    }

    // - Role checking
    const currentUserRole = user._role;
    if (!this.isAuthorized(currentUserRole, authorizedRoles)) {
      if (user._role === Role.ADMIN || user._role === Role.SUPERVISOR) {
        this.router.navigateByUrl('/backoffice/dashboard');
      } else {
        this.router.navigateByUrl('/frontoffice/boards');
      }
      return false;
    }

    // - All checks passed. User is granted access to this page.
    return true;
  }

  private isAuthorized (currentRole: Role, roles: Role[]): boolean {
    const filteredRoles = roles.filter(r => {
      return r === currentRole || r === Role.ALL;
    });

    return filteredRoles.length > 0;
  }
}
