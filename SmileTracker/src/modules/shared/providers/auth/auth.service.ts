import { Injectable } from '@angular/core';
import { NetworkService } from '../network/network.service';
import { User } from '../../models/user';
import { MapperService } from '../mappers/mapper.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

public isAuth = false;
public authStatus = '';

  constructor (
    private networkService: NetworkService,
    private mapperService: MapperService
  ) {

  }
  signIn (): Promise<User> {
    return this.networkService.get('/login').then(user => {
      const loggedUser = this.mapperService.mapJsonToSingleUser(user);
      return Promise.resolve(loggedUser);
    }).catch(error => {
      return Promise.reject(error);
    });
  }

  signOut () {
    this.isAuth = false;
  }

}
