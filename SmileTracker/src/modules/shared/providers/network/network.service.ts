import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
/**
 * Network Layer.
 *
 * `NetworkService` is available as an injectable class, with methods to perform HTTP requests.
 */
export class NetworkService {
  constructor (
    private httpClient: HttpClient,
  ) { }

  /**
   * Construct the array of default header sent with every outbound request.
   *
   * @return an `HttpHeader` bucket.
   */
  private getHeaders (options?: NetworkServiceOptions): HttpHeaders {
    const headers: HttpHeaders = new HttpHeaders();

    if (options && options.plain) {
      headers.append('Content-Type', 'text/plain');
      headers.append('Accept', 'text/plain');
    } else {
      headers.append('Content-Type', 'application/json');
      headers.append('Accept', 'application/json');
    }
    // - Handles Authorization token
    if (!options/*  && !options.distant && !options.noAuthentication */) {
      headers.append('Authorization', 'abcde');
    }

    return headers;
  }

  /**
   * Construct a GET request which contain the default headers bucket.
   *
   * @return an `Observable` of the body as an `Object`.
   */
  public get (endpoint: string, options?: NetworkServiceOptions) {
    const url = options && !!options.distant ? endpoint : environment.api.baseUrl + endpoint;
    return this.httpClient.get(url, {
      headers: this.getHeaders(options)
    }).toPromise();
  }

  /**
   * Construct a POST request which contain the default headers bucket.
   *
   * @return an `Observable` of the body as an `Object`.
   */
  public post (endpoint: string, data: any, options?: NetworkServiceOptions) {
    const url = options && !!options.distant ? endpoint : environment.api.baseUrl + endpoint;
    return this.httpClient.post(url, data, {
      headers: this.getHeaders(options)
    }).toPromise();
  }

  /**
   * Construct a PUT request which contain the default headers bucket.
   *
   * @return an `Observable` of the body as an `Object`.
   */
  public put (endpoint: string, data: any, options?: NetworkServiceOptions) {
    const url = options && !!options.distant ? endpoint : environment.api.baseUrl + endpoint;
    return this.httpClient.put(url, data, {
      headers: this.getHeaders(options)
    }).toPromise();
  }

  /**
   * Construct a DELETE request which contain the default headers bucket.
   *
   * @return an `Observable` of the body as an `Object`.
   */
  public delete (endpoint: string, options?: NetworkServiceOptions) {
    const url = options && !!options.distant ? endpoint : environment.api.baseUrl + endpoint;
    return this.httpClient.delete(url, {
      headers: this.getHeaders(options)
    }).toPromise();
  }
}

/**
 * Define the available options for each network request.
 */
export interface NetworkServiceOptions {
  /**
   * Define whether the request is sent to the API (`false`, default) or to a remote,
   * external server (`true`).
   */
  distant?: boolean;

  /**
   * Define whether the request requires an access token (`false`, default) or **must** be
   * requested anonymously (`true`).
   */
  noAuthentication?: boolean;

  /*
  * Define whether the request is intended to return a plain response (`true`) or not (`false`, default).
  * In this latter case, the `application/json` Accept and Content-Type headers will be set.
  */
  plain?: boolean;
}
