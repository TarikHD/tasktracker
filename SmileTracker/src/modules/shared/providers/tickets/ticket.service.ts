import { Injectable } from '@angular/core';
import { NetworkService } from '../network/network.service';
import { Ticket } from '../../models/ticket';
import { MapperService } from '../mappers/mapper.service';

@Injectable({
  providedIn: 'root'
})
export class TicketsService {

  constructor (
    private networkService: NetworkService,
    private mapperService: MapperService
    ) { }

}
