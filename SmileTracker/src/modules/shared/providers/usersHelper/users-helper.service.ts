import { Injectable, EventEmitter } from '@angular/core';
import { User } from '../../models/user';

@Injectable({
  providedIn: 'root'
})
export class UsersHelperService {

  public users: Array<User> = null;
  public usersEventEmitter = new EventEmitter<Array<User>>();

  constructor () { }

  getUsers (): Array<User> {
    return this.users;
  }

  setUsers (usersArray: Array<User>) {
    this.users = usersArray;
    this.usersEventEmitter.emit(this.users);
  }
}
