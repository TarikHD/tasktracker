import { Injectable } from '@angular/core';
import { NetworkService } from '../network/network.service';
import { Project } from '../../models/project';
import { MapperService } from '../mappers/mapper.service';
import { Ticket } from '../../models/ticket';

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {

  constructor (
    private networkService: NetworkService,
    private mapperService: MapperService
    ) { }

  getProjects (): Promise<Array<Project>> {
    return this.networkService.get('/projects').then(projects => {
      const arrayProjects: Array<Project> = this.mapperService.mapJsonToProjects(projects);

      return Promise.resolve(arrayProjects);
    }).catch(error => {
      return Promise.reject(error);
    });
  }

  public addProject (payload): Promise<Project> {
    return this.networkService.post('/projects', payload)
    .then(data => {
      const project = this.mapperService.mapJsonToSingleProject(data);
      return Promise.resolve(project);
    })
    .catch(error => {
      return Promise.reject(error);
    });
  }

  public editProject (id: number, payload: any): Promise<Project> {
    return this.networkService.put('/project/' + id, payload)
    .then(data => {
      const project = this.mapperService.mapJsonToSingleProject(data);
      return Promise.resolve(project);
    })
    .catch(error => {
      return Promise.reject(error);
    });
  }

  public addTicket (payload): Promise<Ticket> {
    return this.networkService.post('/projects', payload)
    .then(data => {
      const ticket = this.mapperService.mapJsonToAssignedSingleTickets(data);
      return Promise.resolve(ticket);
    })
    .catch(error => {
      return Promise.reject(error);
    });
  }

}
