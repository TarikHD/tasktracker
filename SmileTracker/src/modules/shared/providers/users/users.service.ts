import { Injectable } from '@angular/core';
import { User } from '../../models/user';
import { NetworkService } from '../network/network.service';
import { MapperService } from '../mappers/mapper.service';
@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor (
    private networkService: NetworkService,
    private mapperService: MapperService
    ) { }

  public getUsers (): Promise<Array<User>> {
    return this.networkService.get('/users')
    .then(users => {
      const arrayUsers: Array<User> = this.mapperService.mapJsonToUsers(users);
      return Promise.resolve(arrayUsers);
    }).catch(error => {
      return Promise.reject(error);
    });
  }

  public addUser (payload): Promise<User> {
    return this.networkService.post('/users', payload)
    .then(data => {
      const user = this.mapperService.mapJsonToSingleUser(data);
      return Promise.resolve(user);
    })
    .catch(error => {
      return Promise.reject(error);
    });
  }

  public editUser (id: number, payload: any): Promise<User> {
    return this.networkService.put('/user/' + id, payload)
    .then(data => {
      const user = this.mapperService.mapJsonToSingleUser(data);
      return Promise.resolve(user);
    })
    .catch(error => {
      return Promise.reject(error);
    });
  }

  }
