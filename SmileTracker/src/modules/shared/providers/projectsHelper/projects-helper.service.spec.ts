import { TestBed } from '@angular/core/testing';

import { ProjectsHelperService } from './projects-helper.service';

describe('ProjectsHelperService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProjectsHelperService = TestBed.get(ProjectsHelperService);
    expect(service).toBeTruthy();
  });
});
