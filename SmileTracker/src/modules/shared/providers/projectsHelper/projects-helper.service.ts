import { Injectable, EventEmitter } from '@angular/core';
import { Project } from '../../models/project';

@Injectable({
  providedIn: 'root'
})
export class ProjectsHelperService {

  projects: Array<Project> = null;
  projectsEventEmitter = new EventEmitter<Array<Project>>();

  constructor () { }

  setProjects (projectsArray: Array<Project>) {
    this.projects = projectsArray;
    this.projectsEventEmitter.emit(this.projects);
  }

  getProjects (): Array<Project> {
    return this.projects;
  }
}
