import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { PubSubService } from 'angular7-pubsub';

@Component({
  selector: 'app-delete-confirm-dialog',
  templateUrl: './delete-confirm-dialog.component.html',
  styleUrls: ['./delete-confirm-dialog.component.scss']
})
export class DeleteConfirmDialogComponent implements OnInit {
  isDeleted = false;
  constructor (
    public dialogRef: MatDialogRef<DeleteConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private pubSub: PubSubService
    ) { }

    ngOnInit () {
    }

    onCancel () {
      this.dialogRef.close();
    }

    deleteItem (item) {

      if (this.data) {
        // Making sure that we delete a user by checking lastname/firstname attributes.
        if (this.data && this.data.myData && (this.data.myData.lastname || this.data.myData.firstname)) {
          this.pubSub.$pub('user:deleted', {item});
          this.dialogRef.close();

          // Closing dialog after triggering method.
        } else if (this.data && this.data.myData && this.data.myData.projectName) {
          this.pubSub.$pub('project:deleted', {item});
          this.dialogRef.close();

          // Closing dialog after triggering method.
        } else if (this.data && this.data.ticket && this.data.ticket.label) {
          this.pubSub.$pub('ticket:deleted', {item});
          this.dialogRef.close({isDeleted: true});
        }
      }
    }
  }
