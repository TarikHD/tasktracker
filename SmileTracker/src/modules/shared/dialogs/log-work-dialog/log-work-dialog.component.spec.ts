import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogWorkDialogComponent } from './log-work-dialog.component';

describe('LogWorkDialogComponent', () => {
  let component: LogWorkDialogComponent;
  let fixture: ComponentFixture<LogWorkDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogWorkDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogWorkDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
