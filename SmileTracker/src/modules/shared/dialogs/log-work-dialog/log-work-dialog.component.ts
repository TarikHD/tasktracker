import { Component, OnInit, Inject, ViewChild, ElementRef, OnChanges, AfterViewInit } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA, MatInput } from '@angular/material';
import { LoggedTime } from '../../models/loggedTime';
import { PubSubService } from 'angular7-pubsub';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MapperService } from '../../providers/mappers/mapper.service';
import { _MatTabHeaderMixinBase } from '@angular/material/tabs/typings/tab-header';

@Component({
  selector: 'app-log-work-dialog',
  templateUrl: './log-work-dialog.component.html',
  styleUrls: ['./log-work-dialog.component.scss'],
})
export class LogWorkDialogComponent implements OnInit {
  loggedTimeArray: Array<LoggedTime> = [];
  loggedTime: LoggedTime = new LoggedTime();
  logWorkForm = new FormGroup({
    loggedTime: new FormControl(0.125, [Validators.required]),
    message: new FormControl('', [Validators.required])
  });

  constructor (
    public dialogRef: MatDialogRef<LogWorkDialogComponent>,
    public dialog: MatDialog,
    public pubsub: PubSubService,
    public mapperService: MapperService,
    @Inject(MAT_DIALOG_DATA) public data: any
    ) {}

  ngOnInit () {
  }

  saveLoggedTime () {
    const loggedTime = new LoggedTime();

    if (!this.logWorkForm.valid) {
      return;
    }

    loggedTime.id = this.data.ticket.time.logged.length + 1;
    loggedTime.userId = this.data.ticket.assignedUserId;
    loggedTime.loggedTime = this.logWorkForm.get('loggedTime').value;
    loggedTime.message = this.logWorkForm.get('message').value;
    this.loggedTimeArray.push(this.mapperService.mapJsonToSingleLoggedTime(loggedTime));

    this.dialogRef.close({
      currentLogged: loggedTime,
      data: this.loggedTimeArray, // TODO: Remove if unused
      id: this.data.ticket.id
    });
  }

  public fieldHasError (field: string, error: string): boolean {
    const control = this.logWorkForm.get(field);
    return (
      !!(control && control.invalid && (control.dirty || control.touched)) &&
      control.errors[error]
      );
  }
}
