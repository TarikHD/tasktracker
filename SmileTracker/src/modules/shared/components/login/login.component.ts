import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../providers/auth/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Role } from '../../enums/role';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  public userName: string = null;
  public password: string = null;
  public authStatus: boolean;

  constructor (
    private router: Router,
    private authService: AuthService
  ) { }

  public loginForm = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
  });

  ngOnInit () {
  }

  onLogin () {
    this.userName = this.loginForm.get('username').value;
    this.password = this.loginForm.get('password').value;

    this.authService.signIn().then((user) => {
      if (user) {
        if (user.role === Role.DEVELOPPER) {
          this.router.navigateByUrl('/frontoffice/boards');
        } else if (user.role === 'ADMIN' || user.role === 'SUPERVISOR') {
          this.router.navigateByUrl('/backoffice/dashboard');
        }
        // Set logged user in local storage
        localStorage.setItem('currentUser', JSON.stringify(user));
      }
    });
  }

}
