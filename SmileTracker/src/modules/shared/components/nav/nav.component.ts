import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from '../../models/user';
import { Project } from '../../models/project';
import { SettingsDialogComponent } from 'src/modules/back-office/dialogs/settings-dialog/settings-dialog.component';
import { ActiveHistoryDialogComponent } from 'src/modules/back-office/dialogs/active-history-dialog/active-history-dialog.component';
import { MatDialog, MatSidenav } from '@angular/material';
import { environment } from 'src/environments/environment';
import { ExportDataDialogComponent } from 'src/modules/back-office/dialogs/export-data-dialog/export-data-dialog.component';
import { Router } from '@angular/router';
import { PubSubService } from 'angular7-pubsub';
import { ProjectsService } from '../../providers/projects/projects.service';
import { ProjectsHelperService } from '../../providers/projectsHelper/projects-helper.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
})
export class NavComponent implements OnInit {
  @ViewChild('sidenav') sidenav: MatSidenav;

  users: Array<User> = [];
  projects: Array<Project> = [];
  opened: boolean;
  genericError: string = null;
  viewActiveHistory = false;
  viewSettings = false;
  viewImport = false;

  hasBackdrop: true;

  constructor (
    public dialog: MatDialog,
    public router: Router,
    public events: PubSubService,
    public projectService: ProjectsService,
    public projectsHelperService: ProjectsHelperService
  ) { }

  ngOnInit () {
  }

  // Get user fullName
  getFullName (): string {
    const user = JSON.parse(localStorage.getItem('currentUser'));

    return user ? user._firstname + ' ' + user._lastname : '';
  }

  getUserRole (): string {
    const user = JSON.parse(localStorage.getItem('currentUser'));

    return user ? user._role : null;
  }

  openSettingsDialog () {
    if (!this.viewSettings) {
      const settingsDialog = this.dialog.open(SettingsDialogComponent, {
        width: environment.settingsDialog.width,
        panelClass: 'settings',
        data: {
          opened: true
        },
      });
      settingsDialog.afterOpened().subscribe(() => {
        this.viewSettings = true;
      });
      settingsDialog.afterClosed().subscribe(() => {
        this.viewSettings = false;
      });
    }
  }

  openActiveHistoryDialog () {
    if (!this.viewActiveHistory) {
      const activeHistoryDialog = this.dialog.open(ActiveHistoryDialogComponent, {
        width: environment.activeHistoryDialog.width,
        height: '80%',
        panelClass: 'active-history',
        disableClose: true,
        hasBackdrop: false,
        data: {
        },
      });
      activeHistoryDialog.afterOpened().subscribe(() => {
        this.viewActiveHistory = true;
      });
      activeHistoryDialog.afterClosed().subscribe(() => {
        this.viewActiveHistory = false;
      });
    }
  }

  openExportDataDialog () {
    if (!this.viewImport) {
      const exportDataDialog = this.dialog.open(ExportDataDialogComponent, {
        width: environment.rightDialogs.width,
        height: environment.rightDialogs.height,
        panelClass: 'export-data'
      });
      exportDataDialog.afterOpened().subscribe(() => {
        this.viewImport = true;
      });
      exportDataDialog.afterClosed().subscribe(() => {
        this.viewImport = false;
      });

    }
  }

  close () {
    this.sidenav.close();
  }

  logout () {
    this.projectsHelperService.setProjects(null);
    this.router.navigateByUrl('/login');
  }
}
