import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DeleteConfirmDialogComponent } from '../../dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { ProjectsDialogComponent } from 'src/modules/back-office/dialogs/projects-dialog/projects-dialog.component';
import { UsersDialogComponent } from 'src/modules/back-office/dialogs/users-dialog/users-dialog.component';
import { environment } from 'src/environments/environment';
import { PubSubService } from 'angular7-pubsub';

@Component({
  selector: 'app-table-content',
  templateUrl: './table-content.component.html',
  styleUrls: ['./table-content.component.scss']
})

export class TableContentComponent implements OnInit, OnChanges {
  @Input() title: string = null;
  @Input() data: any = null;
  loadingIndicator = false;
  rows = [];
  filtredArray = [];
  columns = [];

  constructor (
    private dialog: MatDialog,
    private pubsub: PubSubService,
  ) { }

  ngOnInit () {
    this.pubsub.$sub('refresh:table', () => {
      this.rows = [...this.data.rows];
      this.filtredArray = [...this.data.rows];
    });
  }

  ngOnChanges () {
    this.filtredCopy();
    if (this.title && this.data) {
      this.filtredArray = [...this.data.rows];
    }
  }

  getArray () {
    return this.filtredArray;
  }

  openDialogUsers () {
    const dialogRefUser = this.dialog.open(UsersDialogComponent, {
      width: environment.rightDialogs.width,
      height: environment.rightDialogs.height,
      panelClass: 'user-add-modal',
      autoFocus: false,
      data: {
        mode: 'add'
      }
    });

    // TODO: get back here when API is ready.
    dialogRefUser.afterClosed();
  }

  openDialogProjects () {
    const dialogRefProjects = this.dialog.open(ProjectsDialogComponent, {
      width: environment.rightDialogs.width,
      height: environment.rightDialogs.height,
      panelClass: 'project-add-modal',
      autoFocus: false,
      data: {
        mode: 'add'
      }
    });

    // TODO: get back here when API is ready.
    dialogRefProjects.afterClosed();
  }

  displayUserDetails (item) {
    const dialogRef = this.dialog.open(UsersDialogComponent, {
      width: environment.rightDialogs.width,
      height: environment.rightDialogs.height,
      panelClass: 'user-add-modal',
      autoFocus: false,
      data: {
        user: item,
        mode: 'details'
      }
    });

    // TODO: get back here when API is ready.
    dialogRef.afterClosed();
  }

  displayProjectDetails (item) {
    const dialogRef = this.dialog.open(ProjectsDialogComponent, {
      width: environment.rightDialogs.width,
      height: environment.rightDialogs.height,
      panelClass: 'project-add-modal',
      autoFocus: false,
      data: {
        project: item,
        mode: 'details'
      }
    });

    // TODO: get back here when API is ready.
    dialogRef.afterClosed();
  }

  editData (item) {
    if (item.projectName) {
      const dialogRefEdit = this.dialog.open(ProjectsDialogComponent, {
        width: environment.rightDialogs.width,
        height: environment.rightDialogs.height,
        panelClass: 'project-add-modal',
        autoFocus: false,
        data: {
          project: item,
          mode: 'edit'
        }
      });

      // TODO: get back here when API is ready.
      dialogRefEdit.afterClosed();
    } else if (item.firstname && item.lastname) {
      const dialogRefEdit = this.dialog.open(UsersDialogComponent, {
        width: environment.rightDialogs.width,
        height: environment.rightDialogs.height,
        panelClass: 'user-add-modal',
        autoFocus: false,
        data: {
          user: item,
          mode: 'edit'
        }
      });

      // TODO: get back here when API is ready.
      dialogRefEdit.afterClosed();
    }
  }

  // Helpers to get single label in lowercase (user/project)
  getName (label) {
    return label.slice(0, label.length - 1).toLowerCase();
  }

  getRole (role) {
    return `url('../assets/imgs/'${role.toLowerCase()}.png)`;
  }

  openDeleteConfirmationDialog (item) {
    const dialogRefDelete = this.dialog.open(DeleteConfirmDialogComponent, {
      width: environment.deleteDialog.width,
      panelClass: 'delete-confirmation',
      data: {
        myData: item,
      }
    });
    // TODO: get back when API is ready
    dialogRefDelete.afterClosed();
  }

  filtredCopy () {
    this.filtredArray = [...this.data.rows];
  }

  filtredData (value: string) {
    if (!value) {
      this.filtredCopy();
    }
    this.filtredArray = [...this.data.rows].filter(item => {
      if (item.firstname && item.lastname) {
        const fullName = `${item.firstname} ${item.lastname}`;

        if (fullName.toLowerCase().includes(value.trim().toLowerCase())) {
          return item;
        }
      } else if (item.projectName) {
        const projectName = `${item.projectName}`;

        if (projectName.toLowerCase().includes(value.trim().toLowerCase())) {
          return item;
        }
      }
    });
  }
}
