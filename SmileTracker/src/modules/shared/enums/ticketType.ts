export enum ticketType {
  CHORE = 'CHORE',
  IMPR = 'IMPR',
  FEAT = 'FEAT',
  BUGFIX = 'BUGFIX'
}
