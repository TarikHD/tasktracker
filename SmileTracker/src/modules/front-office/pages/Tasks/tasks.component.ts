import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Ticket } from 'src/modules/shared/models/ticket';
import { User } from 'src/modules/shared/models/user';
import { Status } from 'src/modules/shared/enums/status';
import { Project } from 'src/modules/shared/models/project';
import { TicketsDialogComponent } from 'src/modules/back-office/dialogs/tickets-dialog/tickets-dialog.component';
import { PubSubService } from 'angular7-pubsub';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { environment } from 'src/environments/environment';
import { MapperService } from 'src/modules/shared/providers/mappers/mapper.service';
import { ticketType } from 'src/modules/shared/enums/ticketType';
import { ProjectsHelperService } from 'src/modules/shared/providers/projectsHelper/projects-helper.service';
import { UsersHelperService } from 'src/modules/shared/providers/usersHelper/users-helper.service';
import { Update } from 'src/modules/shared/models/update';
import * as moment from 'moment';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit, OnDestroy {
  public requestInProgress = false;
  public genericError: string = null;
  public routeProjectId: number = null;
  public selectedProjectID = 0;

  public listTickets: Array<Ticket> = [];
  public listProjects: Array<Project> = [];
  public listUsers: Array<User> = [];

  public ticketsFound: Array<Ticket> = null;
  public projectSelected: any = [];
  public listAssignedUsers: Array<any> = [];
  // Ticket Status variables
  public doneStatus = Status.DONE;
  public todoStatus = Status.TODO;
  public canceledStatus = Status.CANCELED;
  public codeReviewStatus = Status.CODE_REVIEW;
  public inProgress = Status.IN_PROGRESS;
  // Ticket Importance variables
  public FEAT = ticketType.FEAT;
  public BUGFIX = ticketType.BUGFIX;
  public IMPR = ticketType.IMPR;
  public CHORE = ticketType.CHORE;

  constructor (
    // public snackBar: MatSnackBarModule,
    private projectsHelperService: ProjectsHelperService,
    private usersHelperService: UsersHelperService,
    private mapperService: MapperService,
    private pubsubSvc: PubSubService,
    private dialog: MatDialog,
  ) {

    // this.routeProjectId = Number(this.route.snapshot.params.id);
  }

  ngOnInit () {
    this.ticketsSubscribers();
    this.getListOfUsers();
    this.getListOfProjects();
  }

  ngOnDestroy () {
    // this.dragDrop.reset();
  }

  public ticketsSubscribers () {
    this.pubsubSvc.$sub('new:ticket', (data) => {
      if (data.type && data.type === 'add') {
        return;
      } else {
        // Edit mode
        const newAssignedTickets = this.ticketsFound.map(ticket => {
          if (data.id === ticket.id) {
            data.data.id = ticket.id;
            ticket = this.mapperService.mapJsonToAssignedSingleTickets(data.data);
          }
          return ticket;
        });

        this.ticketsFound = newAssignedTickets;

        const newArrayProjects = this.projectsHelperService.getProjects().filter(project => {
          if (project.id === this.projectSelected.id) {
            project.assignedTickets = this.ticketsFound;
            return project;
          } else {
            return project;
          }
        });

        this.projectsHelperService.setProjects(newArrayProjects);
      }
    });

    // Delete selected ticket from ticketsFound array
    this.pubsubSvc.$sub('ticket:deleted', (data) => {
      this.ticketsFound = this.ticketsFound.filter(ticket => {
        if (ticket.label !== data.item.label) {
          return ticket;
        }
      });
    });
  }

  public getListOfUsers () {
    this.usersHelperService.usersEventEmitter.subscribe(data => {
      this.listUsers = data;
      // this.listAssignedUsers = data;
      this.projectChanged(1);
    });
  }

  public getListOfProjects () {
    this.projectsHelperService.projectsEventEmitter.subscribe(data => {
      this.requestInProgress = false;
      this.listProjects = data;
      this.fillArrays();
    });
  }

  public fillArrays () {
    if (this.projectsHelperService.getProjects()) {
      this.projectsHelperService.getProjects().map((project, index) => {
        if (this.routeProjectId) {
          // Get id from route depending on what project was selected
/*           if (project.id === this.routeProjectId) {
            this.selectedProjectID = project.id;
            this.ticketsFound = project.assignedTickets;
            this.listTickets = project.assignedTickets;
            this.projectSelected = project;
          }
 */        }

        if (index === 0) {
          this.selectedProjectID = project.id;
          this.ticketsFound = project.assignedTickets;
          this.listTickets = project.assignedTickets;
          this.projectSelected = project;
        }
      });
    }

  }

  public remapArray (): void {

    // Re-mappin each ticket to add importance degree as number to sort.
    this.ticketsFound.map(ticket => {
      if (ticket.importance === 'average') {
        ticket.importanceValue = 1;
      }
      if (ticket.importance === 'critical') {
        ticket.importanceValue = 3;
      }
      if (ticket.importance === 'hight') {
        ticket.importanceValue = 2;
      }
      if (ticket.importance === 'minimal') {
        ticket.importanceValue = 0;
      }
    });

    // Sorting array.
    this.ticketsFound = this.ticketsFound.sort((a, b) => {
      const val1 = a.importanceValue;
      const val2 = b.importanceValue;
      if (val1 < val2) {
        return 1;
      }
      if (val1 > val2) {
        return -1;
      }
      return 0;
    });

    this.listTickets = this.ticketsFound;
  }

  // Drag & Drop Script
  public drop (event: CdkDragDrop<any[]>, type) {
    this.ticketsFound.map(ticket => {
      const updatesArray = [];
      if (event.item.data.id === ticket.id) {
        // Assign updates only if theres changes
        const update: Update = new Update();
        update.id = ticket.updates && ticket.updates.length + 1;
        update.status = type;
        update.editionDate = moment(new Date()).format('YYYY-MM-DD HH:m:ss');
        update.assignedUserId = ticket.assignedUserId;

        update.description = '<p><b>' + ticket.label + '</b> was changed from ' +
          ticket.status + ' to ' + type + '</p>';
        // Check if comment exit before pushing in Array
        if (update) {
          updatesArray.push(update);
        }
      }

      // Storing DATA in payload
      const payload = {
        status: ticket.status,
        label: ticket.label,
        assignedUserId: ticket.assignedUserId,
        importance: ticket.importance,
        type: ticket.type,
        importanceValue: this.getImportanceValue(ticket.importanceValue),
        reference: ticket.reference,
        description: ticket.description,
        comments: ticket.comments,
        attachments: ticket.attachments,
        time: ticket.time,
        creationDate: ticket.creationDate,
        updates: ticket.updates ? [...ticket.updates, ...updatesArray] : updatesArray
      };

      this.pubsubSvc.$pub('new:ticket', {
        type,
        data: payload,
        id: ticket.id
      });
    });

    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      this.ticketsFound.map(ticket => {
        if (event.item.data.id === ticket.id) {
          ticket.status = type;
        }
      });

      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }
  }

  public getImportanceValue (importanceLabel): number {
    switch (importanceLabel) {
      case 'minimal':
        return 0;
      case 'average':
        return 1;
      case 'high':
        return 2;
      case 'critical':
        return 3;
    }
  }

  // Project selection Method
  public projectChanged (e) {
    this.listAssignedUsers = [];
    this.listProjects.map((project) => {
      if (project.id === e) {
        this.ticketsFound = project.assignedTickets;
        this.selectedProjectID = project.id;
        this.projectSelected = project;
        this.projectSelected.assignedUsers.map(userId => {
          this.usersHelperService.getUsers().map(user => {
            if (userId === user.id) {
              this.listAssignedUsers.push(user);
            }
          });
        });
        // Re-mappin each ticket to add importance degree as number to sort.
        this.remapArray();
      }
    });
  }

  // Search Block Method
  public onKey (event: any) {
    if (event) {
      this.ticketsFound = this.listTickets.filter(ticket => {
        if (ticket.label.toLowerCase().indexOf(event.toLowerCase()) > -1 ||
          ticket.reference.toLowerCase().indexOf(event.toLowerCase()) > -1) {
          return ticket;
        }
      });
    } else {
      this.ticketsFound = this.listTickets;
    }
  }

  // Looping through ticketsarray to fill in each column.
  public getArrayWithStatus (status: string): Array<Ticket> {
    if (this.ticketsFound) {
      const array = [];
      for (const ticket of this.ticketsFound) {
        if (ticket.status === status) {
          array.push(ticket);
        }
      }
      return array;
    }
  }

  // Show Details Method
  public showDetails (ticketItem: Ticket) {
    this.dialog.open(TicketsDialogComponent, {
      width: environment.rightDialogs.width,
      height: environment.rightDialogs.height,
      panelClass: 'ticket-add-modal',
      disableClose: false,
      autoFocus: false,
      data: {
        source: 'frontoffice',
        mode: 'edit',
        project: this.projectSelected,
        users: this.listUsers,
        ticket: ticketItem,
      },
    });
  }

  // Get type ticket class to set bg Color
  public getTypeClass (type): string {
    return type;
  }
}
