import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from 'src/modules/shared/providers/users/users.service';
import { ProjectsService } from 'src/modules/shared/providers/projects/projects.service';
import { UsersHelperService } from 'src/modules/shared/providers/usersHelper/users-helper.service';
import { ProjectsHelperService } from 'src/modules/shared/providers/projectsHelper/projects-helper.service';

@Component({
  selector: 'app-front-office',
  templateUrl: './front-office.component.html',
  styleUrls: ['./front-office.component.scss']
})
export class FrontOfficeComponent implements OnInit {
  constructor (
    private router: Router,
    public projectsHelperService: ProjectsHelperService,
    public usersHelperService: UsersHelperService,
    public projectsService: ProjectsService,
    public usersService: UsersService
  ) {
    this.projectsService.getProjects().then(projectsArray => {
      this.projectsHelperService.setProjects(projectsArray);
    }).catch(error => {
      // TODO : Make a call to the snackbar and display error
    });

    this.usersService.getUsers().then(usersArray => {
      this.usersHelperService.setUsers(usersArray);
    }).catch(error => {
      // TODO : Display error
    });
  }

  ngOnInit () {
    if (this.router.url === '/frontoffice') {
      this.router.navigateByUrl('/frontoffice/boards');
    }
  }
}
