import * as moment from 'moment';

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  api: {
    baseUrl: 'http://localhost:3001/rest/v1/smileTracker'
  },
  rightDialogs: {
    width: '100%',
    height: '100%'
  },
  leftDialogs: {
    width: '100%',
    height: '100%'
  },
  deleteDialog: {
    width: '400px'
  },
  logWorkDialog: {
    width: '350px'
  },
  settingsDialog: {
    width: '80%'
  },
  activeHistoryDialog: {
    height: '100%',
    width: '400px'
  },
  exports: {
    pdf: 'ExportHistory-' + moment(new Date()).format('YYYY-%%-DD') + '.pdf',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
