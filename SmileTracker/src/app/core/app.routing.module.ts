import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from '../../modules/shared/components/login/login.component';
import { BackOfficeComponent } from 'src/modules/back-office/back-office/back-office.component';
import { UserListComponent } from 'src/modules/back-office/pages/user-list/user-list.component';
import { DashboardComponent } from 'src/modules/back-office/pages/dashboard/dashboard.component';
import { ProjectListComponent } from 'src/modules/back-office/pages/project-list/project-list.component';
import { FrontOfficeComponent } from 'src/modules/front-office/front-office/front-office.component';
import { TasksComponent } from 'src/modules/front-office/pages/Tasks/tasks.component';
import { AuthGuardService as AuthGuard } from 'src/modules/shared/providers/auth/auth.guard';
import { Role } from 'src/modules/shared/enums/role';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },

  {
    path: 'backoffice',
    component: BackOfficeComponent,
    children: [
      {
        path: 'users',
        component: UserListComponent,
        canActivate: [AuthGuard],
        data: {
          roles: [Role.ADMIN, Role.SUPERVISOR]
        }
      },
      {
        path: 'dashboard',
        component: DashboardComponent,
        canActivate: [AuthGuard],
        data: {
          roles: [Role.ADMIN, Role.SUPERVISOR]
        }
      },
      {
        path: 'projects',
        component: ProjectListComponent,
        canActivate: [AuthGuard],
        data: {
          roles: [Role.ADMIN, Role.SUPERVISOR]
        }
      },
      {
        path: '',
        component: BackOfficeComponent,
        canActivate: [AuthGuard],
        data: {
          roles: [Role.ADMIN, Role.SUPERVISOR]
        }
      }
    ]
  },

  {
    path: 'frontoffice',
    component: FrontOfficeComponent, children: [
      {
        path: 'boards',
        component: TasksComponent,
        canActivate: [AuthGuard],
        data: {
          roles: [Role.ADMIN, Role.SUPERVISOR, Role.DEVELOPPER]
        }
      },
      {
        path: '',
        component: FrontOfficeComponent,
        canActivate: [AuthGuard],
        data: {
          roles: [Role.ADMIN, Role.SUPERVISOR, Role.DEVELOPPER]
        }
      }
    ]
  },

  {
    path: '',
    component: LoginComponent,
    canActivate: [AuthGuard],
    data: {
      roles: [Role.ADMIN, Role.SUPERVISOR, Role.DEVELOPPER]
    }
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
