// Contain only Angular Material Module
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatButtonModule, MatCardModule, MatInputModule, MatTableModule, MatOptionModule, MatSelectModule,
  MatToolbarModule, MatMenuModule, MatIconModule, MatFormFieldModule, MatDialogModule, MatDividerModule,
  MatListModule, MatDatepickerModule, MatNativeDateModule, MatChipsModule, MatSidenavModule, MatCheckboxModule, MatTooltipModule
} from '@angular/material';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { CdkTreeModule } from '@angular/cdk/tree';

@NgModule({
  imports: [
    CommonModule,
    MatToolbarModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatTableModule,
    MatMenuModule,
    MatIconModule,
    MatFormFieldModule,
    MatOptionModule,
    MatSelectModule,
    MatDialogModule,
    MatDividerModule,
    MatListModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatChipsModule,
    MatSidenavModule,
    MatCheckboxModule,
    MatTooltipModule,
    DragDropModule,
    CdkTreeModule
  ],

  exports: [
    MatToolbarModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatTableModule,
    MatMenuModule,
    MatIconModule,
    MatFormFieldModule,
    MatOptionModule,
    MatSelectModule,
    MatDialogModule,
    MatDividerModule,
    MatListModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatChipsModule,
    MatSidenavModule,
    MatCheckboxModule,
    MatTooltipModule,
    DragDropModule,
    CdkTreeModule
  ],

  providers: [
    MatDatepickerModule
  ]
})

export class MaterialModule { }
