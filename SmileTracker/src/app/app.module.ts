import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxUploaderModule } from 'ngx-uploader';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { MomentModule } from 'ngx-moment';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PubSubModule } from 'angular7-pubsub';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { AppRoutingModule } from './core/app.routing.module';
import { MaterialModule } from './core/material.module';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '@angular/cdk/layout';
import {
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatRippleModule,
  MatButtonToggleModule
} from '@angular/material';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ExportAsModule } from 'ngx-export-as';

// Shared
// Component
import { AppComponent } from './app.component';
import { LoginComponent } from '../modules/shared/components/login/login.component';
import { NavComponent } from '../modules/shared/components/nav/nav.component';
import { TableContentComponent } from '../modules/shared/components/table-content/table-content.component';

// Dialogs
import { DeleteConfirmDialogComponent } from '../modules/shared/dialogs/delete-confirm-dialog/delete-confirm-dialog.component';
import { LogWorkDialogComponent } from '../modules/shared/dialogs/log-work-dialog/log-work-dialog.component';

// Back Office
import { BackOfficeComponent } from '../modules/back-office/back-office/back-office.component';
// Component
import { DualListBoxComponent } from '../modules/back-office/components/dual-list-box/dual-list-box.component';
import { TicketBoxComponent } from '../modules/back-office/components/ticket-box/ticket-box.component';
// Pages
import { UserListComponent } from '../modules/back-office/pages/user-list/user-list.component';
import { DashboardComponent } from '../modules/back-office/pages/dashboard/dashboard.component';
import { ProjectListComponent } from '../modules/back-office/pages/project-list/project-list.component';

// Dialogs
import { UsersDialogComponent } from '../modules/back-office/dialogs/users-dialog/users-dialog.component';
import { ProjectsDialogComponent } from '../modules/back-office/dialogs/projects-dialog/projects-dialog.component';
import { TicketsDialogComponent } from '../modules/back-office/dialogs/tickets-dialog/tickets-dialog.component';
import { SettingsDialogComponent } from '../modules/back-office/dialogs/settings-dialog/settings-dialog.component';
import { ActiveHistoryDialogComponent } from '../modules/back-office/dialogs/active-history-dialog/active-history-dialog.component';
import { ExportDataDialogComponent } from '../modules/back-office/dialogs/export-data-dialog/export-data-dialog.component';
import { HistoryDialogComponent } from 'src/modules/back-office/dialogs/history-dialog/history-dialog.component';

// Front Office
import { FrontOfficeComponent } from '../modules/front-office/front-office/front-office.component';
// Providers
import { ProjectsService } from 'src/modules/shared/providers/projects/projects.service';
import { UsersService } from 'src/modules/shared/providers/users/users.service';
import { MapperService } from 'src/modules/shared/providers/mappers/mapper.service';
import { TasksComponent } from 'src/modules/front-office/pages/Tasks/tasks.component';
import { ProjectsHelperService } from 'src/modules/shared/providers/projectsHelper/projects-helper.service';
import { UsersHelperService } from 'src/modules/shared/providers/usersHelper/users-helper.service';
import { AuthService } from 'src/modules/shared/providers/auth/auth.service';
import { AuthGuardService } from 'src/modules/shared/providers/auth/auth.guard';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavComponent,
    BackOfficeComponent,
    FrontOfficeComponent,
    UserListComponent,
    TableContentComponent,
    DashboardComponent,
    ProjectListComponent,
    UsersDialogComponent,
    ProjectsDialogComponent,
    DualListBoxComponent,
    DeleteConfirmDialogComponent,
    TicketBoxComponent,
    TicketsDialogComponent,
    LogWorkDialogComponent,
    SettingsDialogComponent,
    ActiveHistoryDialogComponent,
    ExportDataDialogComponent,
    HistoryDialogComponent,
    TasksComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FlexLayoutModule,
    NgxDatatableModule,
    NgxUploaderModule,
    MomentModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    PubSubModule.forRoot(),
    CommonModule,
    PerfectScrollbarModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatRippleModule,
    NgxChartsModule,
    MatButtonToggleModule,
    ExportAsModule
  ],
  entryComponents: [
    UsersDialogComponent,
    ProjectsDialogComponent,
    TicketsDialogComponent,
    DeleteConfirmDialogComponent,
    LogWorkDialogComponent,
    SettingsDialogComponent,
    ActiveHistoryDialogComponent,
    ExportDataDialogComponent,
    HistoryDialogComponent
  ],
  providers: [
    MapperService,
    ProjectsService,
    UsersService,
    ProjectsHelperService,
    UsersHelperService,
    AuthService,
    AuthGuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
